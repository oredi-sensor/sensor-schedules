# Sensor Schedules

## Goal

This project has made to send automatic notifications to the Sayn App and also to provide an REST API to the Daily Highlight feature. The notifications will be sent using the Firebase API service and the REST API has made using the hapi framework.

## Setup

1. **Installation**
   `yarn install` or `npm install`
2. **Set your .env**
   Create the `.env` file in the root directory of your project, then copy the code of the `.env.example`, paste in the `.env` created recently and, finally, update the variables to you what you want.
3. **Run the server**
   `yarn start` or `npm start`

## Technologies

`firebase-admin`: SDK to interact with Firebase API, current we only use the Firebase Cloud Messaging service, with is we can:

1. Send messages to individual devices
2. Send messages to topics and condition statements that match one or more topics.
   Subscribe and unsubscribe devices to and from topics
3. Construct message payloads tailored to different target platforms

`node-schedule`: Node Schedule is a flexible cron-like and not-cron-like job scheduler. It allows you to schedule jobs for execution at specific dates, with optional recurrence rules.
`@hapi/hapi`: The Simple, Secure Framework Developers Trust to develop web API REST.
`jwt`: Handle user token to validate access the API REST requests, also is the way to identify the user and get only your records.
`objection` & `knex`: Even though ORM is the best commonly known acronym to describe objection, a more accurate description is to call it a relational query builder. You get all the benefits of an SQL query builder but also a powerful set of tools for working with relations. Objection.js is built on an SQL query builder called knex. In this project we use the same PostgreSQL database of the main Sensor project. In addition, we use the SQLite3 to store the notifications logs.

## Architecture Diagram

![Architecture Diagram Image](docs/architeture_diagramn.png)

## Structure

- root project
  - **logs:** It's where the Sqlite3 Database file is stored. This dir will be only containing the db.sqlite.
  - **src:**
    - **app:**
      - **controllers:** Triggered by routes, this should call a service or make a logic with an query directly in database.
      - **jobs:** Responsible to process jobs from the scheduler. Is the middleman between the scheduler and the services.
      - **models:** DB table mapping for use in objection ORM.
      - **routes:** Endpoints to REST API.
      - **schedules:** Files that are triggered automatically by `node-schedule`. A schedule is responsible for processing a job, running a service, or processing small logic.
      - **services:** It's where we have the services that we will provide the complex logic for the controller/job layer layer. The purpose of this is to simplify the controller/job code and create shared services that can be used across multiple layers.
      - **validation:** A simple object validation to ensure that the REST API request will have all parameters, payload and query parameters.
    - **config:** It's where we have the configuration to external libs, e.g. firebase, hapi and knex.
    - **app.js:** It's responsible for initializing the hapi server and enhance the server instance with auth plugin, routes, firebase, schedules and logger.
    - **server.js:** It just starts the hapi server.
