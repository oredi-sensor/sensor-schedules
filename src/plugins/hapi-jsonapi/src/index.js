'use strict';

// Load modules
const Glob = require('glob');
const Hoek = require('hoek');
const Joi = require('joi');
const JSONAPI = require('jsonapi-serializer');
const Path = require('path');
const jsonpack = require('jsonpack');

// Declare internals
const internals = {};

// Schemas
internals.schemas = {
  options: Joi.object({
    serializersPattern: Joi.string().required(),
  }),
};

// Defaults
internals.defaults = {
  options: {},
};

// Serializers
internals.serializers = new Map();

/**
 * Serializes data using JSON API.
 *
 * @param type JSON API type.
 * @param data Data.
 * @param options Serializer options.
 * @returns {{}}
 */
internals.jsonapiSerialize = (type, data, options = {}) => {
  // Compute settings
  const settings = (() => {
    if (internals.serializers.has(type)) {
      const buildOptions = internals.serializers.get(type);
      return buildOptions(options.include || []);
    }

    return options;
  })();

  // Instantiate the serializer
  const serializer = new JSONAPI.Serializer(type, settings);

  // Serialize the data
  const response = serializer.serialize(data);

  // Set meta if present
  if (options.meta) {
    response.meta = options.meta;
  }
  // return jsonpack.pack(response);
  return response;
};

/**
 * Generates a JSON API response.
 *
 * @param type JSON API type.
 * @param data Data.
 * @param options Serializer options.
 * @returns {*} Response.
 */
internals.jsonapiResponse = function (type, data, options = {}) {
  const c = {
    include: options.include ? options.include.slice(1, -1) : new Array(0),
  };

  if (options.meta) {
    c.meta = {
      page: options.page ? options.page : 1,
      pageSize: options.rowsByPage,
      rowCount: options.list.total,
      pageCount: Math.ceil(options.list.total / options.rowsByPage),
    };
  }

  return this.response(
    internals.jsonapiSerialize(type, JSON.parse(JSON.stringify(data)), c)
  ).type('application/vnd.api+json');
};

/**
 * Plugin name.
 *
 * @type {string}
 */
exports.name = 'jsonapi';

/**
 * Plugin package.
 */
exports.pkg = require('../package.json');

/**
 * Registers the plugin.
 *
 * @param server Server object.
 * @param options Options object.
 */
exports.register = async (server, options) => {
  // Validate options
  Joi.assert(options, internals.schemas.options);

  const settings = Hoek.applyToDefaults(internals.defaults, options);

  // Register the serializers
  await new Promise((resolve, reject) => {
    Glob(settings.serializersPattern, (err, files) => {
      if (err) {
        server.log(['error', 'jsonapi'], err.message);
        reject(err);
        return;
      }

      files.forEach((file) => {
        const parsedPath = Path.parse(file);

        // Require the serializer definition
        const SerializerBuilder = require(Path.resolve(file));

        // Add the serializer to the map
        internals.serializers.set(parsedPath.name, SerializerBuilder);
      });

      resolve();
    });
  });

  // Expose the JSON API serialize method
  server.method('jsonapi.serialize', internals.jsonapiSerialize);

  // Decorate the response toolkit
  server.decorate('toolkit', 'jsonapi', internals.jsonapiResponse);

  server.log('jsonapi', 'Plugin loaded.');
};
