'use strict';

// Load modules
const chai = require('chai'), should = chai.should();
const Hapi = require('hapi');

// Load Chai plugins
chai.use(require('chai-as-promised'));

// Load Database plugin (test target)
const plugin = require('../');

const internals = {};

internals.options = {
    serializersPattern: 'test/serializers/*.js'
};

describe('JSON API Plugin', function () {

    let server;

    beforeEach('start the hapi server', function () {

        server = new Hapi.Server({
            host: 'localhost'
        });

        return server.start().should.be.fulfilled;
    });

    afterEach('stop the hapi server', function () {

        return server.stop().should.be.fulfilled;
    });

    describe('#register', function () {

        it('should register the plugin', function () {

            return server.register({
                plugin,
                options: internals.options
            }).should.be.fulfilled;
        });

        it('should expose the jsonapi.serialize method', function () {
            return server.register({
                plugin,
                options: internals.options
            }).then(() => {
                server.methods.should.have.property('jsonapi');
                server.methods.jsonapi.should.have.property('serialize');
            });
        });

        it('should decorate the response toolkit', function () {

            return server.register({
                plugin,
                options: internals.options
            }).then(() => {
                server.decorations.toolkit.should.have.members(['jsonapi']);
            });
        });
    });

    describe('response toolkit', function () {

        beforeEach('register the plugin', function () {

            return server.register({
                plugin,
                options: internals.options
            });
        });

        it('should build a response with a known serializer', function () {

            server.route({
                method: 'GET',
                path: '/test',
                handler: (request, h) => h.jsonapi('test', {
                    id: 1,
                    foo: 'bar'
                })
            });

            return server.inject({
                method: 'GET',
                url: '/test'
            }).then((response) => {
                JSON.parse(response.payload).should.have.deep.property('data', {
                    type: 'test',
                    id: 1,
                    attributes: {
                        foo: 'bar'
                    }
                });

                response.headers.should.have.property('content-type', 'application/vnd.api+json');
            });
        });

        it('should build a response with a known serializer', function () {

            server.route({
                method: 'GET',
                path: '/unknown',
                handler: (request, h) => h.jsonapi('unknown', {
                    id: 1,
                    foo: 'bar'
                })
            });

            return server.inject({
                method: 'GET',
                url: '/unknown'
            }).then((response) => {
                JSON.parse(response.payload).should.have.deep.property('data', {
                    type: 'unknowns',
                    id: 1
                });

                response.headers.should.have.property('content-type', 'application/vnd.api+json');
            });
        });

        it('should add meta to a response', function () {

            server.route({
                method: 'GET',
                path: '/test',
                handler: (request, h) => h.jsonapi('test', {
                    id: 1,
                    foo: 'bar'
                }, {
                    meta: {
                        bar: 'foo'
                    }
                })
            });

            return server.inject({
                method: 'GET',
                url: '/test'
            }).then((response) => {
                JSON.parse(response.payload).should.have.deep.property('meta', {
                    bar: 'foo'
                });
            });
        });
    });
});
