'use strict';

// Load modules
const Glob = require('glob');
const Hoek = require('hoek');
const Joi = require('joi');
const Path = require('path');
const jsonpack = require('jsonpack');

// Declare internals
const internals = {};

// Defaults
internals.defaults = {
  options: {},
};

function lowerFirstLetter(string) {
  return string.charAt(0).toLowerCase() + string.slice(1);
}

/**
 * Serializes data using JSON API.
 *
 * @param type JSON API type.
 * @param data Data.
 * @param options Serializer options.
 * @returns {{}}
 */

internals.jsonapiGetTypeOfObject = (instance) => {
  function attrIsValidType(attr) {
    return (
      attr != null &&
      typeof attr === 'object' &&
      attr.constructor &&
      attr.constructor.name !== 'Object' &&
      attr.constructor.name !== 'Date'
    );
  }

  if (instance.constructor.name !== 'Object') {
    instance.__type__ = lowerFirstLetter(instance.constructor.name);
  }

  for (const key in instance) {
    if (instance.hasOwnProperty(key)) {
      const attr = instance[key];

      if (Array.isArray(attr)) {
        const newArray = [];
        for (const subAttr of attr) {
          if (attrIsValidType(subAttr)) {
            newArray.push(internals.jsonapiGetTypeOfObject(subAttr));
          }
        }
        instance[key] = newArray;
      } else if (attrIsValidType(attr)) {
        instance[key] = internals.jsonapiGetTypeOfObject(attr);
      } else if (typeof attr === 'object') {
        // console.log(attr)
      }
    }
  }

  return instance;
};

internals.jsonapiSerialize = (data, options = {}) => {
  // Serialize the data
  let response = { data: null };

  if (Array.isArray(data)) {
    response.data = [];
    for (const instance of data) {
      response.data.push(internals.jsonapiGetTypeOfObject(instance));
    }
  } else {
    response = internals.jsonapiGetTypeOfObject(data);
  }
  // Set meta if present
  if (options.meta) {
    response.meta = options.meta;
  }
  if (options.pack) return jsonpack.pack(response);

  return response;
};

/**
 * Generates a JSON API response.
 *
 * @param type JSON API type.
 * @param data Data.
 * @param options Serializer options.
 * @returns {*} Response.
 */
internals.jsonapiResponse = function (data, options = {}) {
  const c = {};

  if (options.pack) {
    c.pack = true;
  }

  if (options.meta) {
    c.meta = {
      page: options.page ? options.page : 1,
      pageSize: options.rowsByPage,
      rowCount: options.list.total,
      pageCount: Math.ceil(options.list.total / options.rowsByPage),
    };
  }

  return this.response(internals.jsonapiSerialize(data, c)).type(
    'application/vnd.api+json'
  );
};

/**
 * Plugin name.
 *
 * @type {string}
 */
exports.name = 'objectionapi';

/**
 * Plugin package.
 */
exports.pkg = require('../package.json');

/**
 * Registers the plugin.
 *
 * @param server Server object.
 * @param options Options object.
 */
exports.register = async (server, options) => {
  // Validate options
  // Joi.assert(options, internals.schemas.options);

  const settings = Hoek.applyToDefaults(internals.defaults, options);

  // Expose the JSON API serialize method
  server.method('objectionapi.serialize', internals.jsonapiSerialize);

  // Decorate the response toolkit
  server.decorate('toolkit', 'objectionapi', internals.jsonapiResponse);

  server.log('objectionapi', 'Plugin loaded.');
};
