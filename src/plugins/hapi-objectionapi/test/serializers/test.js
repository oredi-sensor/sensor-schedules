'use strict';

const test = () => ({
    attributes: ['foo'],
    pluralizeType: false
});

module.exports = test;
