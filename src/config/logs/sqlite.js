module.exports = {
  client: 'sqlite3',
  useNullAsDefault: true,
  connection: {
    filename: 'logs/db.sqlite',
  },
};
