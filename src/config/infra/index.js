const path = require('path');

const serverConfig = path.join(
  __dirname,
  `${process.env.NODE_ENV || 'development'}.js`
);

module.exports = require(serverConfig)();
