'use strict';

const Fs = require('fs');
const Path = require('path');

module.exports = () => {
  const ca = Fs.readFileSync(Path.join(__dirname, '/ssl/intermediate.crt'));
  const cert = Fs.readFileSync(
    Path.join(__dirname, '/ssl/api.horizontalcities.com.crt')
  );
  const key = Fs.readFileSync(
    Path.join(__dirname, '/ssl/api.horizontalcities.com.key')
  );

  return {
    server: {
      app: {
        environment: 'development',
        tls: {
          ca,
          cert,
          key,
        },
      },
      debug: {
        log: ['error'],
        request: ['error'],
      },
      port: process.env.SERVER_PORT || 8079,
      routes: {
        cors: {
          origin: ['*'],
          additionalHeaders: ['headers'],
        },
      },
    },
    register: {
      plugins: [
        { plugin: 'hapi-require-https' },
        {
          plugin: 'hapi-cors',
          options: {
            origins: ['*'],
            methods: ['POST, GET, OPTIONS', 'PATCH', 'PUT', 'DELETE'],
          },
        },
        {
          plugin: 'database',
          options: {
            knex: {
              client: 'pg',
              connection: {
                host: process.env.BD_HOST,
                port: process.env.BD_PORT,
                user: process.env.BD_USER,
                password: process.env.BD_PASSWORD,
                database: process.env.BD_NAME,
                charset: 'utf8',
                timezone: 'UTC',
              },
            },
            modelsPattern: 'models/**/*.js',
          },
        },
      ],
    },
  };
};
