require('dotenv/config');
const firebaseAdmin = require('firebase-admin');
const glue = require('glue');

const serverConfig = require('./config/infra');
const LogService = require('./app/services/LogService');
const Token = require('./app/models/Token');
require('./app/services/SlackService');

class App {
  async start() {
    this.server = await glue.compose(serverConfig, { relativeTo: __dirname });

    this.auth();

    this.routes();

    this.firebase();

    this.server.start();

    this.schedules();

    this.logger();
  }

  auth() {
    this.server.auth.strategy('auth', 'jwt', {
      jwt: {
        secretOrPublicKey: this.server.settings.app.tls.cert,
        options: {
          algorithms: ['RS256'],
          issuer: 'urn:sensor.com',
        },
        validateFunc: async (_, payload) => {
          try {
            const token = await Token.query().findById(payload.jti);

            if (!token) {
              throw new Error('TOKEN_NOT_FOUND');
            }

            return {
              isValid: true,
              credentials: {
                id: token.id,
                user_id: token.subject_id,
                subject_type: token.subject_type,
              },
            };
          } catch (e) {
            return {
              isValid: false,
              credentials: {},
            };
          }
        },
      },
    });
  }

  routes() {
    this.server.route(require('./app/routes/_index'));
  }

  firebase() {
    firebaseAdmin.initializeApp();
  }

  schedules() {
    require('./app/schedules/_index');
  }

  logger() {
    this.server.events.on('response', (response) => {
      const { payload, params, query, auth } = response;

      if (response.path === '/logs') {
        return;
      }

      LogService.save({
        type: 'request',
        request_data: { payload, params, query },
        user_id: auth?.credentials?.user_id,
        status_code: response.response.statusCode,
        response_data: response.response.source,
      });
    });
  }
}

module.exports = new App();
