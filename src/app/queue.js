const HighlightNotificationJob = require('./jobs/HighlightNotificationJob');

const jobs = [HighlightNotificationJob];

const STATES = {
  STOPPED: 0,
  PROCESSING: 1,
};

class Queue {
  constructor() {
    this.queues = {};
    this.state = {};
  }

  add = (queue, arg) => {
    if (!this.queues[queue]?.length) {
      this.queues[queue] = [];
    }

    this.queues[queue].push(arg);
  };

  addPriority = (queue, arg) => {
    if (!this.queues[queue]?.length) {
      this.queues[queue] = [];
    }

    this.queues[queue].unshift(arg);
  };

  pop = (queue) => {
    if (!this.queues[queue]?.length) {
      return null;
    }

    return this.queues[queue].shift();
  };

  getAll = () => {
    return { ...this.queues };
  };

  async process(queue) {
    if (this.state[queue] === STATES.PROCESSING) {
      return console.log('already processing');
    }

    this.state[queue] = STATES.PROCESSING;
    for (const { key, handle } of jobs) {
      this.processQueue(key, handle);
    }
  }

  async processQueue(key, handle) {
    const arg = this.pop(key);

    if (!arg) {
      this.state[key] = STATES.STOPPED;
      return null;
    }

    console.log('*processQueue*', { arg }, this.queues[key].length);

    try {
      await handle(arg);
    } catch (e) {
      console.log(e);
    }

    this.processQueue(key, handle);
  }
}

module.exports = new Queue();
