const Boom = require('@hapi/boom');

const MobileApp = require('../models/MobileApp/MobileApp');

const DailyHighlightService = require('../services/DailyHighlightService');

exports.getHighlight = async (
  { type, questionnaire_answer_id, language = 'en', user_id },
  { public_key }
) => {
  try {
    const mobileApp = await MobileApp.query().findOne({
      public_token: public_key,
    });

    if (!mobileApp?.id) {
      throw Boom.notFound(
        `Cannot find the Mobile App to public_key (${public_key})`
      );
    }

    const content = await DailyHighlightService.run({
      mobileAppId: mobileApp.id,
      type,
      language,
      questionnaire_answer_id,
      user_id,
    });

    return content;
  } catch (e) {
    throw Boom.badRequest(e.message);
  }
};
