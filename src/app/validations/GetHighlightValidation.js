const Joi = require('Joi');

module.exports = {
  query: Joi.object({
    type: Joi.string().valid(
      'General Insights',
      'Resources of focus insights',
      'Act Quiz call to action',
      'General Criteria',
      'Self Criteria',
      'Social Criteria',
      'Professional Criteria',
      'Report and React',
      'Heart (Re)Question'
    ),
    questionnaire_answer_id: Joi.string().uuid(),
  }),
};
