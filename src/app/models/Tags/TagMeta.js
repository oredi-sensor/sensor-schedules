const { Model } = require('objection');

class TagMeta extends Model {
  static get tableName() {
    return 'tag_meta';
  }

  static get idColumn() {
    return ['tag_id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['tag_id', 'value'],
      properties: {
        tag_id: { type: 'string', format: 'uuid' },
        tag_category_meta_id: { type: 'string', format: 'uuid' },
        value: { type: 'string' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const TagCategoryMeta = require('./TagCategoryMeta');
    const Tag = require('./Tag');

    return {
      tagCategoryMeta: {
        relation: Model.BelongsToOneRelation,
        modelClass: TagCategoryMeta,
        join: {
          from: 'tag_meta.tag_category_meta_id',
          to: 'tag_category_meta.id',
        },
      },
      tag: {
        relation: Model.BelongsToOneRelation,
        modelClass: Tag,
        join: {
          from: 'tag_meta.tag_id',
          to: 'tag.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = TagMeta;
