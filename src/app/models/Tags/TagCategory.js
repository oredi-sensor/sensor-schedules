const { Model } = require('objection');

class TagCategory extends Model {
  static get tableName() {
    return 'tag_category';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        string_resource_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
        has_meta_data: { type: 'boolean' },
      },
    };
  }

  static get relationMappings() {
    const TagCategoryMeta = require('./TagCategoryMeta');
    const Tag = require('./Tag');

    return {
      metas: {
        relation: Model.HasManyRelation,
        modelClass: TagCategoryMeta,
        join: {
          from: 'tag_category.id',
          to: 'tag_category_meta.tag_category_id',
        },
      },
      tags: {
        relation: Model.HasManyRelation,
        modelClass: Tag,
        join: {
          from: 'tag_category.id',
          to: 'tag.tag_category_id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = TagCategory;
