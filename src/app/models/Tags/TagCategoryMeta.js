const { Model } = require('objection');

class TagCategoryMeta extends Model {
  static get tableName() {
    return 'tag_category_meta';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['tag_category_id', 'field_name', 'field_type'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        tag_category_id: { type: 'string', format: 'uuid' },
        field_name: { type: 'string' },
        field_type: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const TagCategory = require('./TagCategory');
    const TagCategoryMetaFieldType = require('./TagCategoryMetaFieldType');
    const TagMeta = require('./TagMeta');

    return {
      tagCategory: {
        relation: Model.BelongsToOneRelation,
        modelClass: TagCategory,
        join: {
          from: 'tag_category_meta.tag_category_id',
          to: 'tag_category.id',
        },
      },
      fieldType: {
        relation: Model.BelongsToOneRelation,
        modelClass: TagCategoryMetaFieldType,
        join: {
          from: 'tag_category_meta.field_type',
          to: 'tag_category_meta_field_type.id',
        },
      },
      tagMetas: {
        relation: Model.HasManyRelation,
        modelClass: TagMeta,
        join: {
          from: 'tag_category_meta.id',
          to: 'tag_meta.tag_category_meta_id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = TagCategoryMeta;
