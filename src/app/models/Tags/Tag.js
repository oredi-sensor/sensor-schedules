const { Model } = require('objection');

class Tag extends Model {
  static get tableName() {
    return 'tag';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['tag_category_id', 'string_resource_id'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        tag_category_id: { type: 'string', format: 'uuid' },
        string_resource_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const TagCategory = require('./TagCategory');
    const StringResources = require('../StringResources');
    const TagMeta = require('./TagMeta');
    const QuestionTag = require('../Questionnaire/QuestionTag');

    return {
      category: {
        relation: Model.BelongsToOneRelation,
        modelClass: TagCategory,
        join: {
          from: 'tag.tag_category_id',
          to: 'tag_category.id',
        },
      },
      stringResources: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'tag.string_resource_id',
          to: 'string_resources.id',
        },
      },
      metas: {
        relation: Model.HasManyRelation,
        modelClass: TagMeta,
        join: {
          from: 'tag.id',
          to: 'tag_meta.tag_id',
        },
      },
      questionTags: {
        relation: Model.HasManyRelation,
        modelClass: QuestionTag,
        join: {
          from: 'tag.id',
          to: 'question_tag.tag_id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = Tag;
