const { Model } = require('objection');

class TagCategoryMetaFieldType extends Model {
  static get tableName() {
    return 'tag_category_meta_field_type';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['type'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        type: { type: 'string' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const TagCategoryMeta = require('./TagCategoryMeta');

    return {
      tagCategoryMetas: {
        relation: Model.HasManyRelation,
        modelClass: TagCategoryMeta,
        join: {
          from: 'tag_category_meta_field_type.id',
          to: 'tag_category_meta.field_type',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = TagCategoryMetaFieldType;
