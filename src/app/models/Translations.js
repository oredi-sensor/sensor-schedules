const { Model } = require('objection');

class Translations extends Model {
  static get tableName() {
    return 'translations';
  }

  static get idColumn() {
    return ['id'];
  }

  static get modifiers() {
    return {
      language(builder, languageCode) {
        builder.leftJoinRelation('language');
        builder.where('language.code', languageCode);
      },
      filterLanguage(builder, languageCode) {
        builder.leftJoinRelation('language');
        builder.where('language.code', languageCode);
      },
    };
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['language_id', 'string_resource_id', 'translation'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        language_id: { type: 'string', format: 'uuid' },
        string_resource_id: { type: 'string', format: 'uuid' },
        translation: { type: 'string' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const Language = require('./Language');
    const StringResources = require('./StringResources');

    return {
      language: {
        relation: Model.BelongsToOneRelation,
        modelClass: Language,
        join: {
          from: 'translations.language_id',
          to: 'language.id',
        },
      },
      stringResources: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'translations.string_resource_id',
          to: 'string_resources.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = Translations;
