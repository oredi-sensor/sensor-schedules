const { Model } = require('objection');

// to encrypt the password
require('objection-password')();

class UserRole extends Model {
  static get tableName() {
    return 'user_role';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['email', 'password'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        role: { type: 'string' },
      },
    };
  }
}

module.exports = UserRole;
