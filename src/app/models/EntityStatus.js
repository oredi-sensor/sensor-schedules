const { Model } = require('objection');

const unique = require('objection-unique')({
  fields: ['status'],
  identifiers: ['id'],
});

class EntityStatus extends unique(Model) {
  static get tableName() {
    return 'entity_status';
  }

  static get idColumn() {
    return 'id';
  }

  static get notDeleted() {
    return {
      notDeleted: (builder) => builder.whereNot('status', 'DELETED'),
    };
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['status'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        status: { type: 'string', minLength: 1, maxLength: 255 },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = EntityStatus;
