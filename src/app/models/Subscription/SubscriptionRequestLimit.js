const { Model } = require('objection');

class SubscriptionRequestLimit extends Model {
  static get tableName() {
    return 'subscription_request_limit';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['amount'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        amount: { type: 'integer' },
        subscription_frequency_id: { type: 'string', format: 'uuid' },
        subscription_id: { type: 'string', format: 'uuid' },
      },
    };
  }

  static get relationMappings() {
    const Subscription = require('./Subscription');
    const SubscriptionFrequency = require('./SubscriptionFrequency');

    return {
      frequency: {
        relation: Model.BelongsToOneRelation,
        modelClass: SubscriptionFrequency,
        join: {
          from: 'subscription_request_limit.subscription_frequency_id',
          to: 'subscription_frequency.id',
        },
      },
      subscription: {
        relation: Model.BelongsToOneRelation,
        modelClass: Subscription,
        join: {
          from: 'subscription_request_limit.subscription_id',
          to: 'subscription.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = SubscriptionRequestLimit;
