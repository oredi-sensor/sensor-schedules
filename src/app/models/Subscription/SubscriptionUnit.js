const { Model } = require('objection');

const unique = require('objection-unique')({
  fields: ['unit'],
  identifiers: ['id'],
});

class SubscriptionUnit extends unique(Model) {
  static get tableName() {
    return 'subscription_unit';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['unit'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        unit: { type: 'string', minLength: 1, maxLength: 40 },
      },
    };
  }

  static get relationMappings() {
    const SubscriptionRequestSummary = require('./SubscriptionRequestSummary');
    const SubscriptionRequestLimit = require('./SubscriptionRequestLimit');

    return {
      subscriptionRequestSummaries: {
        relation: Model.HasManyRelation,
        modelClass: SubscriptionRequestSummary,
        join: {
          from: 'subscription_unit.id',
          to: 'subscription_request_summary.subscription_unit_id',
        },
      },
      subscriptionRequestLimits: {
        relation: Model.HasManyRelation,
        modelClass: SubscriptionRequestLimit,
        join: {
          from: 'subscription_unit.id',
          to: 'subscription_request_limit.subscription_unit_id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = SubscriptionUnit;
