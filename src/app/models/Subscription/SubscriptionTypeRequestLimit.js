const { Model } = require('objection');

class SubscriptionTypeRequestLimit extends Model {
  static get tableName() {
    return 'subscription_type_request_limit';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['amount', 'subscription_type_id', 'subscription_frequency_id'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        amount: { type: 'integer' },
        subscription_frequency_id: { type: 'string', format: 'uuid' },
        subscription_type_id: { type: 'string', format: 'uuid' },
      },
    };
  }

  static get relationMappings() {
    const SubscriptionType = require('./SubscriptionType');
    const SubscriptionFrequency = require('./SubscriptionFrequency');

    return {
      frequency: {
        relation: Model.BelongsToOneRelation,
        modelClass: SubscriptionFrequency,
        join: {
          from: 'subscription_type_request_limit.subscription_frequency_id',
          to: 'subscription_frequency.id',
        },
      },
      subscription: {
        relation: Model.BelongsToOneRelation,
        modelClass: SubscriptionType,
        join: {
          from: 'subscription_type_request_limit.subscription_type_id',
          to: 'subscription_type.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = SubscriptionTypeRequestLimit;
