const { Model } = require('objection');

class SubscriptionRequestSummary extends Model {
  static get tableName() {
    return 'subscription_request_summary';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['amount', 'start_date', 'end_date'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        start_date: { type: 'string', format: 'date-time' },
        end_date: { type: 'string', format: 'date-time' },
        amount: { type: 'integer' },
        subscription_frequency_id: { type: 'string', format: 'uuid' },
        subscription_id: { type: 'string', format: 'uuid' },
      },
    };
  }

  static get relationMappings() {
    const Subscription = require('./Subscription');
    const SubscriptionFrequency = require('./SubscriptionFrequency');

    return {
      frequency: {
        relation: Model.BelongsToOneRelation,
        modelClass: SubscriptionFrequency,
        join: {
          from: 'subscription_request_summary.subscription_frequency_id',
          to: 'subscription_frequency.id',
        },
      },
      subscription: {
        relation: Model.BelongsToOneRelation,
        modelClass: Subscription,
        join: {
          from: 'subscription_request_summary.subscription_id',
          to: 'subscription.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = SubscriptionRequestSummary;
