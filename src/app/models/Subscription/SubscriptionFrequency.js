const { Model } = require('objection');

const unique = require('objection-unique')({
  fields: ['frequency'],
  identifiers: ['id'],
});

class SubscriptionFrequency extends unique(Model) {
  static get tableName() {
    return 'subscription_frequency';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['frequency', 'duration'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        frequency: { type: 'string', minLength: 1, maxLength: 40 },
        duration: { type: 'integer' },
      },
    };
  }

  static get relationMappings() {
    const Subscription = require('./Subscription');
    const SubscriptionRequestSummary = require('./SubscriptionRequestSummary');
    const SubscriptionRequestLimit = require('./SubscriptionRequestLimit');

    return {
      subscriptions: {
        relation: Model.HasManyRelation,
        modelClass: Subscription,
        join: {
          from: 'subscription_frequency.id',
          to: 'subscription.frequecy_id',
        },
      },
      subscriptionRequestSummaries: {
        relation: Model.HasManyRelation,
        modelClass: SubscriptionRequestSummary,
        join: {
          from: 'subscription_frequency.id',
          to: 'subscription_request_summary.subscription_frequency_id',
        },
      },
      subscriptionRequestLimits: {
        relation: Model.HasManyRelation,
        modelClass: SubscriptionRequestLimit,
        join: {
          from: 'subscription_frequency.id',
          to: 'subscription_request_limit.subscription_frequency_id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = SubscriptionFrequency;
