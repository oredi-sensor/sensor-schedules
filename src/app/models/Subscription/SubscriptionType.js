const { Model } = require('objection');

const unique = require('objection-unique')({
  fields: ['type'],
  identifiers: ['id'],
});

class SubscriptionType extends unique(Model) {
  static get tableName() {
    return 'subscription_type';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['type', 'request_price', 'discount'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        status_id: { type: 'string', format: 'uuid' },
        type: { type: 'string', minLength: 1, maxLength: 40 },
        request_price: { type: 'number' },
        discount: { type: 'integer' },
      },
    };
  }

  static get relationMappings() {
    const Status = require('../EntityStatus');
    const Subscription = require('./Subscription');
    const SubscriptionTypeRequestLimit = require('./SubscriptionTypeRequestLimit');

    return {
      status: {
        relation: Model.BelongsToOneRelation,
        modelClass: Status,
        join: {
          from: 'subscription_type.status_id',
          to: 'entity_status.id',
        },
      },
      subscriptions: {
        relation: Model.HasManyRelation,
        modelClass: Subscription,
        join: {
          from: 'subscription_type.id',
          to: 'subscription.type_id',
        },
      },
      requestLimits: {
        relation: Model.HasManyRelation,
        modelClass: SubscriptionTypeRequestLimit,
        join: {
          from: 'subscription_type.id',
          to: 'subscription_type_request_limit.subscription_type_id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = SubscriptionType;
