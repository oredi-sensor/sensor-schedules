const { Model } = require('objection');

class Subscription extends Model {
  static get tableName() {
    return 'subscription';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['start_date', 'end_date', 'request_price', 'total_price'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        start_date: { type: 'string', format: 'date-time' },
        end_date: { type: 'string', format: 'date-time' },
        request_price: { type: 'number' },
        discount: { type: 'integer' },
        total_price: { type: 'number' },
        current_cost: { type: 'number' },
        user_id: { type: 'string', format: 'uuid' },
        status_id: { type: 'string', format: 'uuid' },
        subscription_type_id: { type: 'string', format: 'uuid' },
        subscription_frequency_id: { type: 'string', format: 'uuid' },
      },
    };
  }

  static get relationMappings() {
    const Status = require('../EntityStatus');
    const SubscriptionFrequency = require('./SubscriptionFrequency');
    const SubscriptionType = require('./SubscriptionType');
    const SubscriptionRequestLimit = require('./SubscriptionRequestLimit');
    const SubscriptionRequestSummary = require('./SubscriptionRequestSummary');
    const Client = require('../User');
    const SubscriptionRequestLog = require('./SubscriptionRequestLog');

    return {
      status: {
        relation: Model.BelongsToOneRelation,
        modelClass: Status,
        join: {
          from: 'subscription.status_id',
          to: 'entity_status.id',
        },
      },
      type: {
        relation: Model.BelongsToOneRelation,
        modelClass: SubscriptionType,
        join: {
          from: 'subscription.subscription_type_id',
          to: 'subscription_type.id',
        },
      },
      frequency: {
        relation: Model.BelongsToOneRelation,
        modelClass: SubscriptionFrequency,
        join: {
          from: 'subscription.subscription_frequency_id',
          to: 'subscription_frequency.id',
        },
      },
      requestLimits: {
        relation: Model.HasManyRelation,
        modelClass: SubscriptionRequestLimit,
        join: {
          from: 'subscription.id',
          to: 'subscription_request_limit.subscription_id',
        },
      },
      requestsSummary: {
        relation: Model.HasManyRelation,
        modelClass: SubscriptionRequestSummary,
        join: {
          from: 'subscription.id',
          to: 'subscription_request_summary.subscription_id',
        },
      },
      User: {
        relation: Model.BelongsToOneRelation,
        modelClass: Client,
        join: {
          from: 'subscription.user_id',
          to: 'user.id',
        },
      },
      log: {
        relation: Model.HasManyRelation,
        modelClass: SubscriptionRequestLog,
        join: {
          from: 'subscription.id',
          to: 'subscription_request_log.subscription_id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = Subscription;
