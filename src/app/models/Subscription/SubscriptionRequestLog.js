const { Model } = require('objection');

class SubscriptionRequestLog extends Model {
  static get tableName() {
    return 'subscription_request_log';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['success'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        error: { type: ['string', 'null'], minLength: 1, maxLength: 255 },
        success: { type: 'boolean' },
        cost: { type: 'number' },
        subscription_id: { type: 'string', format: 'uuid' },
      },
    };
  }

  static get relationMappings() {
    const Subscription = require('./Subscription');

    return {
      subscription: {
        relation: Model.BelongsToOneRelation,
        modelClass: Subscription,
        join: {
          from: 'subscription_request_log.subscription_id',
          to: 'subscription.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = SubscriptionRequestLog;
