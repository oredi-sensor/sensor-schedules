const { Model } = require('objection');

class Image extends Model {
  static get tableName() {
    return 'image';
  }

  static get idColumn() {
    return ['id'];
  }

  static get modifiers() {
    return {
      onlyId(builder) {
        builder.select('id');
      },
    };
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['image'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        image: { type: 'text' },
        image_name: { type: 'string' },
        image_meta_data: { type: 'string', format: 'uuid' },
      },
    };
  }

  static get relationMappings() {
    const ImageMetaData = require('./MetaData/ImageMetaData');

    return {
      criteria: {
        relation: Model.ManyToManyRelation,
        modelClass: ImageMetaData,
        join: {
          from: 'image.id',
          through: {
            from: 'image_meta_data.image_id',
            to: 'image_meta_data.criteria_id',
          },
          to: 'alg_criteria.id',
        },
      },
    };
  }

  /*
    $beforeInsert() {
        this.created_at = new Date().toISOString();
    }

    $beforeUpdate() {
        this.updated_at = new Date().toISOString();
    }
*/
}

module.exports = Image;
