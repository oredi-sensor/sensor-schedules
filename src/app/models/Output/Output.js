const { Model } = require('objection');

class Output extends Model {
  static get tableName() {
    return 'output';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        name: { type: 'string', format: 'uuid' },
        output_use_case: { type: 'varchar' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const EntityStatus = require('../EntityStatus');
    const StringResources = require('../StringResources');
    const Image = require('../Image');
    const Video = require('../Video');
    const UserDailyOutput = require('../Alg/UserDailyOutput');
    const Criteria = require('../Alg/AlgCriteria');
    const AlgElement = require('../Alg/AlgElement');
    const MetaSkill = require('../Alg/AlgMetaSkill');
    const Principle = require('../Alg/AlgPrinciple');
    const Feeling = require('../Alg/Feeling');
    const Organ = require('../Alg/Organ');
    const Season = require('../Alg/Season');
    const NaturalElement = require('../Alg/NaturalElement');
    const MobileAppOutput = require('../MobileApp/MobileAppOutput');
    const MobileAppEntityComponent = require('../MobileApp/MobileAppEntityComponent');
    const Questionnaire = require('../Questionnaire/Questionnaire');

    return {
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'output.status_id',
          to: 'entity_status.id',
        },
      },
      texts: {
        relation: Model.ManyToManyRelation,
        // Solution 2:
        //
        // Absolute file path to a module that exports the model class.
        // This is similar to solution 1, but objection calls `require`
        // under the hood. The downside here is that you need to give
        // an absolute file path because of the way `require` works.
        modelClass: StringResources,
        join: {
          from: 'output.id',
          through: {
            // persons_movies is the join table.
            from: 'output_text.output_id',
            to: 'output_text.text_id',
            extra: ['index'],
          },
          to: 'string_resources.id',
        },
      },
      images: {
        relation: Model.ManyToManyRelation,
        // Solution 2:
        //
        // Absolute file path to a module that exports the model class.
        // This is similar to solution 1, but objection calls `require`
        // under the hood. The downside here is that you need to give
        // an absolute file path because of the way `require` works.
        modelClass: Image,
        join: {
          from: 'output.id',
          through: {
            // persons_movies is the join table.
            from: 'output_image.output_id',
            to: 'output_image.image_id',
            extra: ['index'],
          },
          to: 'image.id',
        },
      },
      videos: {
        relation: Model.ManyToManyRelation,
        // Solution 2:
        //
        // Absolute file path to a module that exports the model class.
        // This is similar to solution 1, but objection calls `require`
        // under the hood. The downside here is that you need to give
        // an absolute file path because of the way `require` works.
        modelClass: Video,
        join: {
          from: 'output.id',
          through: {
            // persons_movies is the join table.
            from: 'output_video.output_id',
            to: 'output_video.video_id',
            extra: ['index'],
          },
          to: 'video.id',
        },
      },
      nameOutput: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'output.name',
          to: 'string_resources.id',
        },
      },

      criteria: {
        relation: Model.ManyToManyRelation,
        modelClass: Criteria,
        join: {
          from: 'output.id',
          through: {
            from: 'output_meta_data.output_id',
            to: 'output_meta_data.criteria_id',
            extra: ['action', 'validation'],
          },
          to: 'alg_criteria.id',
        },
      },
      metaSkills: {
        relation: Model.ManyToManyRelation,
        modelClass: MetaSkill,
        join: {
          from: 'output.id',
          through: {
            from: 'output_meta_data.output_id',
            to: 'output_meta_data.meta_skill_id',
            extra: ['action'],
          },
          to: 'alg_meta_skill.id',
        },
      },
      principles: {
        relation: Model.ManyToManyRelation,
        modelClass: Principle,
        join: {
          from: 'output.id',
          through: {
            from: 'output_meta_data.output_id',
            to: 'output_meta_data.principle_id',
            extra: ['action'],
          },
          to: 'alg_principle.id',
        },
      },
      feelings: {
        relation: Model.ManyToManyRelation,
        modelClass: Feeling,
        join: {
          from: 'output.id',
          through: {
            from: 'output_meta_data.output_id',
            to: 'output_meta_data.feeling_id',
            extra: ['action'],
          },
          to: 'feeling.id',
        },
      },
      organs: {
        relation: Model.ManyToManyRelation,
        modelClass: Organ,
        join: {
          from: 'output.id',
          through: {
            from: 'output_meta_data.output_id',
            to: 'output_meta_data.organ_id',
            extra: ['action'],
          },
          to: 'organ.id',
        },
      },
      seasons: {
        relation: Model.ManyToManyRelation,
        modelClass: Season,
        join: {
          from: 'output.id',
          through: {
            from: 'output_meta_data.output_id',
            to: 'output_meta_data.season_id',
            extra: ['action'],
          },
          to: 'season.id',
        },
      },
      naturalElements: {
        relation: Model.ManyToManyRelation,
        modelClass: NaturalElement,
        join: {
          from: 'output.id',
          through: {
            from: 'output_meta_data.output_id',
            to: 'output_meta_data.natural_element_id',
            extra: ['action'],
          },
          to: 'natural_element.id',
        },
      },
      questionnaires: {
        relation: Model.ManyToManyRelation,
        modelClass: Questionnaire,
        join: {
          from: 'output.id',
          through: {
            from: 'output_meta_data.output_id',
            to: 'output_meta_data.questionnaire_id',
            extra: ['action'],
          },
          to: 'questionnaire.id',
        },
      },
      elements: {
        relation: Model.ManyToManyRelation,
        modelClass: AlgElement,
        join: {
          from: 'output.id',
          through: {
            to: 'element_attribute.element_id',
            from: 'element_attribute.output_id',
          },
          to: 'alg_elements.id',
        },
      },
      dailyOutputs: {
        relation: Model.HasManyRelation,
        modelClass: UserDailyOutput,
        join: {
          from: 'output.id',
          to: 'app_user_daily_output.output_id',
        },
      },
      mobileAppOutput: {
        relation: Model.HasManyRelation,
        modelClass: MobileAppOutput,
        join: {
          from: 'output.id',
          to: 'mobile_app_output.output_id',
        },
      },
      entityComponent: {
        relation: Model.BelongsToOneRelation,
        modelClass: MobileAppEntityComponent,
        join: {
          from: 'output.id',
          to: 'mobile_app_entity_component.entity_id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = Output;
