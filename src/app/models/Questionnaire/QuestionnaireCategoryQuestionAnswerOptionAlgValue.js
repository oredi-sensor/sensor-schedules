const { Model } = require('objection');

class QuestionnaireCategoryQuestionAnswerOptionAlgValue extends Model {
  static get tableName() {
    return 'questionnaire_category_question_answer_option_alg_value';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        questionnaire_category_question_id: { type: 'string', format: 'uuid' },
        answer_option_id: { type: 'string', format: 'uuid' },
        value: { type: 'integer' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const AnswerOption = require('./AnswerOption');
    const QuestionnaireCategoryQuestion = require('./QuestionnaireCategoryQuestion');

    return {
      questionnaireCategoryQuestion: {
        relation: Model.BelongsToOneRelation,
        modelClass: QuestionnaireCategoryQuestion,
        join: {
          from:
            'questionnaire_category_question_criteria.questionnaire_category_question_id',
          to: 'questionnaire_category_question.id',
        },
      },
      answerOptions: {
        relation: Model.BelongsToOneRelation,
        modelClass: AnswerOption,
        join: {
          from:
            'questionnaire_category_question_answer_option_alg_value.answer_option_id',
          to: 'answer_option.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = QuestionnaireCategoryQuestionAnswerOptionAlgValue;
