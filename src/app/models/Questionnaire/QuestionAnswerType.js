const { Model } = require('objection');

class QuestionAnswerType extends Model {
  static get tableName() {
    return 'question_answer_type';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['question_answer_type'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        question_answer_type: { type: 'string' },
        target_table: { type: 'string' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const Question = require('./Question');

    return {
      questions: {
        relation: Model.HasManyRelation,
        modelClass: Question,
        join: {
          from: 'question_answer_type.id',
          to: 'question.question_answer_type_id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = QuestionAnswerType;
