const { Model } = require('objection');

class Invite extends Model {
  static get tableName() {
    return 'invite';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['user_id', 'questionnaire_answer_id', 'email'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        s_id: { type: 'integer' },
        user_id: { type: 'string', format: 'uuid' },
        questionnaire_answer_id: { type: 'string', format: 'uuid' },
        email: {
          type: 'string',
          format: 'email',
          minLength: 5,
          maxLength: 255,
        }, // todo check email format
        status_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const EntityStatus = require('../EntityStatus');
    const User = require('../User');
    const QuestionnaireAnswer = require('./QuestionnaireAnswer');

    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'invite.user_id',
          to: 'user.id',
        },
      },
      questionnaireAnswer: {
        relation: Model.BelongsToOneRelation,
        modelClass: QuestionnaireAnswer,
        join: {
          from: 'invite.questionnaire_answer_id',
          to: 'questionnaire_answer.id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'invite.status_id',
          to: 'entity_status.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }
}

module.exports = Invite;
