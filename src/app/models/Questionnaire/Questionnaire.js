const { Model } = require('objection');

class Questionnaire extends Model {
  static get tableName() {
    return 'questionnaire';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        name: { type: 'string', format: 'uuid' },
        description: { type: ['string', 'null'], format: 'uuid' },
        image_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
        s_id: { type: 'integer' },
        status_id: { type: 'string', format: 'uuid' },
      },
    };
  }

  static get relationMappings() {
    const StringResources = require('../StringResources');
    const QuestionnaireCategory = require('./QuestionnaireCategory');
    const EntityStatus = require('../EntityStatus');

    return {
      stringResources: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'questionnaire.name',
          to: 'string_resources.id',
        },
      },
      descriptions: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'questionnaire.description',
          to: 'string_resources.id',
        },
      },
      categories: {
        relation: Model.HasManyRelation,
        modelClass: QuestionnaireCategory,
        join: {
          from: 'questionnaire.id',
          to: 'questionnaire_category.questionnaire_id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'questionnaire.status_id',
          to: 'entity_status.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = Questionnaire;
