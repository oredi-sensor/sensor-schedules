const { Model } = require('objection');

class QuestionEffect extends Model {
  static get tableName() {
    return 'question_effect';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        effect_param: { type: 'string' },
        effect_type: { type: 'string' },
        created_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const QuestionEffectType = require('./QuestionEffectType');

    return {
      effectType: {
        relation: Model.BelongsToOneRelation,
        modelClass: QuestionEffectType,
        join: {
          from: 'question_effect.effect_type',
          to: 'question_effect_type.effect_type',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }
}

module.exports = QuestionEffect;
