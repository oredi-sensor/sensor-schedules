const { Model } = require('objection');

class QuestionEffectType extends Model {
  static get tableName() {
    return 'question_effect_type';
  }

  static get idColumn() {
    return ['effect_type'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        effect_type: { type: 'string' },
        created_at: { type: 'timestamp' },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }
}

module.exports = QuestionEffectType;
