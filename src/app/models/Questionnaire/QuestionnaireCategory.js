const { Model } = require('objection');

class QuestionnaireCategory extends Model {
  static get tableName() {
    return 'questionnaire_category';
  }

  static get idColumn() {
    return ['id'];
  }

  static get modifiers() {
    return {
      notDeleted(builder) {
        builder.leftJoinRelation('entityStatus');
        builder.whereNot('entityStatus.status', 'DELETED');
      },
      orderByIndex(builder) {
        builder.orderBy('index', 'asc');
      },
    };
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        name: { type: 'string', format: 'uuid' },
        index: { type: 'integer' },
        questionnaire_id: { type: 'string', format: 'uuid' },
        principle_id: {
          anyOf: [{ type: 'null' }, { type: 'string', format: 'uuid' }],
        },
        status_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const StringResources = require('../StringResources');
    const Questionnaire = require('./Questionnaire');
    const QuestionnaireCategoryQuestion = require('./QuestionnaireCategoryQuestion');
    const EntityStatus = require('../EntityStatus');
    const Principle = require('../Alg/AlgPrinciple');

    return {
      stringResources: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'questionnaire_category.name',
          to: 'string_resources.id',
        },
      },
      questionnaire: {
        relation: Model.BelongsToOneRelation,
        modelClass: Questionnaire,
        join: {
          from: 'questionnaire_category.questionnaire_id',
          to: 'questionnaire.id',
        },
      },
      questions: {
        relation: Model.HasManyRelation,
        modelClass: QuestionnaireCategoryQuestion,
        join: {
          from: 'questionnaire_category.id',
          to: 'questionnaire_category_question.questionnaire_category_id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'questionnaire_category.status_id',
          to: 'entity_status.id',
        },
      },
      principle: {
        relation: Model.BelongsToOneRelation,
        modelClass: Principle,
        join: {
          from: 'questionnaire_category.principle_id',
          to: 'alg_principle.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = QuestionnaireCategory;
