const { Model } = require('objection');

class QuestionnaireCategoryQuestionCriteria extends Model {
  static get tableName() {
    return 'questionnaire_category_question_criteria';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        questionnaire_category_question_id: { type: 'string', format: 'uuid' },
        criteria_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const AlgCriteria = require('../Alg/AlgCriteria');
    const QuestionnaireCategoryQuestion = require('./QuestionnaireCategoryQuestion');

    return {
      questionnaireCategoryQuestion: {
        relation: Model.BelongsToOneRelation,
        modelClass: QuestionnaireCategoryQuestion,
        join: {
          from:
            'questionnaire_category_question_criteria.questionnaire_category_question_id',
          to: 'questionnaire_category_question.id',
        },
      },
      criteria: {
        relation: Model.BelongsToOneRelation,
        modelClass: AlgCriteria,
        join: {
          from: 'questionnaire_category_question_criteria.criteria_id',
          to: 'alg_criteria.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = QuestionnaireCategoryQuestionCriteria;
