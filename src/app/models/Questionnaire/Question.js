const { Model } = require('objection');

class Question extends Model {
  static get tableName() {
    return 'question';
  }

  static get idColumn() {
    return ['id'];
  }

  static get modifiers() {
    return {
      notDeleted(builder) {
        builder.leftJoinRelation('entityStatus');
        builder.whereNot('entityStatus.status', 'DELETED');
      },
      orderByIndex(builder) {
        builder.orderBy('questionnaire_category_question.index', 'asc');
      },
    };
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [
        'question_answer_type_id',
        'is_open',
        'is_multiple',
        'status_id',
        'created_by',
      ],
      properties: {
        id: { type: 'string', format: 'uuid' },
        question: { type: 'string', format: 'uuid' },
        status_id: { type: 'string', format: 'uuid' },
        created_by: { type: 'string', format: 'uuid' },
        question_answer_type_id: { type: 'string', format: 'uuid' },
        // mobile_answer_component_id: {type: ["string", "null"], format: "uuid"},
        is_open: { type: 'boolean' },
        is_multiple: { type: 'boolean' },
        required: { type: 'boolean' },
        index: { type: 'integer' },
        s_id: { type: 'integer' },
        effect_id: { type: ['string', 'null'], format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const User = require('../User');
    const QuestionTag = require('./QuestionTag');
    const QuestionnaireCategoryQuestion = require('./QuestionnaireCategoryQuestion');
    const EntityStatus = require('../EntityStatus');
    const StringResources = require('../StringResources');
    const Image = require('../Image');
    const Video = require('../Video');
    const QuestionAnswerType = require('./QuestionAnswerType');
    const AnswerOption = require('./AnswerOption');
    const MobileAnswerComponent = require('./MobileAnswerComponent');
    const QuestionEffect = require('./QuestionEffect');
    const MobileAppEntityComponent = require('../MobileApp/MobileAppEntityComponent');

    return {
      text: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'question.question',
          to: 'string_resources.id',
        },
      },
      effect: {
        relation: Model.BelongsToOneRelation,
        modelClass: QuestionEffect,
        join: {
          from: 'question.effect_id',
          to: 'question_effect.id',
        },
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'question.created_by',
          to: 'user.id',
        },
      },
      tags: {
        relation: Model.HasManyRelation,
        modelClass: QuestionTag,
        join: {
          from: 'question.id',
          to: 'question_tag.question_id',
        },
      },
      questionnaireCategoryQuestion: {
        relation: Model.HasManyRelation,
        modelClass: QuestionnaireCategoryQuestion,
        join: {
          from: 'question.id',
          to: 'questionnaire_category_question.question_id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'question.status_id',
          to: 'entity_status.id',
        },
      },
      mobileAnswerComponent: {
        relation: Model.BelongsToOneRelation,
        modelClass: MobileAnswerComponent,
        join: {
          from: 'question.mobile_answer_component_id',
          to: 'mobile_answer_component.id',
        },
      },
      answerType: {
        relation: Model.BelongsToOneRelation,
        modelClass: QuestionAnswerType,
        join: {
          from: 'question.question_answer_type_id',
          to: 'question_answer_type.id',
        },
      },
      answerOptions: {
        relation: Model.ManyToManyRelation,
        modelClass: AnswerOption,
        join: {
          from: 'question.id',
          through: {
            from: 'question_answer_option.question_id',
            to: 'question_answer_option.answer_option_id',
            extra: ['index'],
          },
          to: 'answer_option.id',
        },
      },
      images: {
        relation: Model.ManyToManyRelation,
        // Solution 2:
        //
        // Absolute file path to a module that exports the model class.
        // This is similar to solution 1, but objection calls `require`
        // under the hood. The downside here is that you need to give
        // an absolute file path because of the way `require` works.
        modelClass: Image,
        join: {
          from: 'question.id',
          through: {
            // persons_movies is the join table.
            from: 'question_image.question_id',
            to: 'question_image.image_id',
          },
          to: 'image.id',
        },
      },
      subTexts: {
        relation: Model.ManyToManyRelation,
        // Solution 2:
        //
        // Absolute file path to a module that exports the model class.
        // This is similar to solution 1, but objection calls `require`
        // under the hood. The downside here is that you need to give
        // an absolute file path because of the way `require` works.
        modelClass: StringResources,
        join: {
          from: 'question.id',
          through: {
            // persons_movies is the join table.
            from: 'question_text.question_id',
            to: 'question_text.text_id',
          },
          to: 'string_resources.id',
        },
      },
      videos: {
        relation: Model.ManyToManyRelation,
        // Solution 2:
        //
        // Absolute file path to a module that exports the model class.
        // This is similar to solution 1, but objection calls `require`
        // under the hood. The downside here is that you need to give
        // an absolute file path because of the way `require` works.
        modelClass: Video,
        join: {
          from: 'question.id',
          through: {
            // persons_movies is the join table.
            from: 'question_video.question_id',
            to: 'question_video.video_id',
          },
          to: 'video.id',
        },
      },
      entityComponent: {
        relation: Model.BelongsToOneRelation,
        modelClass: MobileAppEntityComponent,
        join: {
          from: 'question.id',
          to: 'mobile_app_entity_component.entity_id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = Question;
