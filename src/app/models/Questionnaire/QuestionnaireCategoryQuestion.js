const { Model } = require('objection');

class QuestionnaireCategoryQuestion extends Model {
  static get tableName() {
    return 'questionnaire_category_question';
  }

  static get idColumn() {
    return ['id'];
  }

  static get modifiers() {
    return {
      notDeleted(builder) {
        builder.leftJoinRelation('question.entityStatus');
        builder.leftJoinRelation('entityStatus');
        builder.whereNot('entityStatus.status', 'DELETED');
        builder.whereNot('question:entityStatus.status', 'DELETED');
      },
      orderByIndex(builder) {
        builder.orderBy('index', 'asc');
      },
    };
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        questionnaire_category_id: { type: 'string', format: 'uuid' },
        question_id: { type: 'string', format: 'uuid' },
        required: { type: 'boolean' },
        status_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
        index: { type: 'integer' },
      },
    };
  }

  static get relationMappings() {
    const Question = require('./Question');
    const QuestionnaireCategory = require('./QuestionnaireCategory');
    const QuestionnaireCategoryQuestionCriteria = require('./QuestionnaireCategoryQuestionCriteria');
    const QuestionnaireCategoryQuestionAnswerOptionAlgValue = require('./QuestionnaireCategoryQuestionAnswerOptionAlgValue');
    const EntityStatus = require('../EntityStatus');

    return {
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'questionnaire_category_question.status_id',
          to: 'entity_status.id',
        },
      },
      question: {
        relation: Model.BelongsToOneRelation,
        modelClass: Question,
        join: {
          from: 'questionnaire_category_question.question_id',
          to: 'question.id',
        },
      },
      questionnaireCategory: {
        relation: Model.BelongsToOneRelation,
        modelClass: QuestionnaireCategory,
        join: {
          from: 'questionnaire_category_question.questionnaire_category_id',
          to: 'questionnaire_category.id',
        },
      },
      questionCriteria: {
        relation: Model.HasManyRelation,
        modelClass: QuestionnaireCategoryQuestionCriteria,
        join: {
          from: 'questionnaire_category_question.id',
          to:
            'questionnaire_category_question_criteria.questionnaire_category_question_id',
        },
      },
      answerOptionAlgValue: {
        relation: Model.HasManyRelation,
        modelClass: QuestionnaireCategoryQuestionAnswerOptionAlgValue,
        join: {
          from: 'questionnaire_category_question.id',
          to:
            'questionnaire_category_question_answer_option_alg_value.questionnaire_category_question_id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = QuestionnaireCategoryQuestion;
