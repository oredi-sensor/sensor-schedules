const { Model } = require('objection');

class QuestionnaireAnswer extends Model {
  static get tableName() {
    return 'questionnaire_answer';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        completed: { type: 'boolean' },
        questionnaire_id: { type: 'string', format: 'uuid' },
        current_category: { type: ['string', null], format: 'uuid' },
        user_id: { type: 'string', format: 'uuid' },
        s_id: { type: 'integer' },
        invited: { type: 'boolean' },
        is_highlight: { type: 'boolean' },
        token: { type: 'string' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
        status_id: { type: 'string', format: 'uuid' },
        mobile_app_id: { type: 'string', format: 'uuid' },
      },
    };
  }

  static get relationMappings() {
    // const StringResources = require("../StringResources");
    const QuestionnaireCategory = require('./QuestionnaireCategory');
    const EntityStatus = require('../EntityStatus');
    const User = require('../User');
    const Questionnaire = require('./Questionnaire');
    const Answers = require('./UserQuestionnaireAnswers');
    const Invites = require('./Invite');
    const MobileApp = require('../MobileApp/MobileApp');

    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'questionnaire_answer.user_id',
          to: 'user.id',
        },
      },
      currentCategory: {
        relation: Model.BelongsToOneRelation,
        modelClass: QuestionnaireCategory,
        join: {
          from: 'questionnaire_answer.current_category',
          to: 'questionnaire_category.id',
        },
      },
      questionnaire: {
        relation: Model.BelongsToOneRelation,
        modelClass: Questionnaire,
        join: {
          from: 'questionnaire_answer.questionnaire_id',
          to: 'questionnaire.id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'questionnaire_answer.status_id',
          to: 'entity_status.id',
        },
      },
      mobileApp: {
        relation: Model.BelongsToOneRelation,
        modelClass: MobileApp,
        join: {
          from: 'questionnaire_answer.mobile_app_id',
          to: 'mobile_app.id',
        },
      },

      answers: {
        relation: Model.HasManyRelation,
        modelClass: Answers,
        join: {
          from: 'questionnaire_answer.id',
          to: 'user_questionnaire_answers.questionnaire_answer_id',
        },
      },

      invites: {
        relation: Model.HasManyRelation,
        modelClass: Invites,
        join: {
          from: 'questionnaire_answer.id',
          to: 'invite.questionnaire_answer_id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = QuestionnaireAnswer;
