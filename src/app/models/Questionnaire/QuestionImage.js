const { Model } = require('objection');

class QuestionImage extends Model {
  static get tableName() {
    return 'question_image';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        image_id: { type: 'string', format: 'uuid' },
        question_id: { type: 'string', format: 'uuid' },
      },
    };
  }

  static get relationMappings() {
    const Question = require('./Question');
    const Images = require('../Image');

    return {
      question: {
        relation: Model.BelongsToOneRelation,
        modelClass: Question,
        join: {
          from: 'question_image.question_id',
          to: 'question.id',
        },
      },
      image: {
        relation: Model.BelongsToOneRelation,
        modelClass: Images,
        // filter: query => query.select('id', 'image_name'),
        join: {
          from: 'question_image.image_id',
          to: 'image.id',
        },
      },
    };
  }
  /*
        $beforeInsert() {
            this.created_at = new Date().toISOString();
        }

        $beforeUpdate() {
            this.updated_at = new Date().toISOString();
        }
        */
}

module.exports = QuestionImage;
