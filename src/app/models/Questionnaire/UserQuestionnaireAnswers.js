const { Model } = require('objection');

class UserQuestionnaireAnswers extends Model {
  static get tableName() {
    return 'user_questionnaire_answers';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['question_id'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        questionnaire_id: { type: ['string', null], format: 'uuid' },
        question_id: { type: 'string', format: 'uuid' },
        question_tags: { type: 'json' },
        is_open: { type: 'boolean' },
        is_highlight: { type: 'boolean' },
        is_multiple: { type: 'boolean' },
        image_answer: { type: ['string', 'null'], format: 'uuid' },
        text_answer: { type: ['string', 'null'] },
        closed_answer_id: { type: ['string', 'null'], format: 'uuid' },
        user_id: { type: 'string', format: 'uuid' },
        questionnaire_category_id: { type: ['string', null], format: 'uuid' },
        questionnaire_answer_id: { type: ['string', null], format: 'uuid' },
        checked: { type: ['boolean', 'null'] },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const Image = require('../Image');
    // const QuestionAnswerOption = require("./QuestionAnswerOption");
    const AnswerOption = require('./AnswerOption');
    const Question = require('./Question');
    const QuestionnaireCategory = require('./QuestionnaireCategory');
    const Questionnaire = require('./Questionnaire');
    const User = require('../User');
    const QuestionnaireAnswer = require('./QuestionnaireAnswer');

    return {
      image: {
        relation: Model.BelongsToOneRelation,
        modelClass: Image,
        join: {
          from: 'user_questionnaire_answers.image_answer',
          to: 'image.id',
        },
      },
      answerOption: {
        relation: Model.BelongsToOneRelation,
        modelClass: AnswerOption,
        join: {
          from: 'user_questionnaire_answers.closed_answer_id',
          to: 'answer_option.id',
        },
      },
      question: {
        relation: Model.BelongsToOneRelation,
        modelClass: Question,
        join: {
          from: 'user_questionnaire_answers.question_id',
          to: 'question.id',
        },
      },
      questionnaireCategory: {
        relation: Model.BelongsToOneRelation,
        modelClass: QuestionnaireCategory,
        join: {
          from: 'user_questionnaire_answers.questionnaire_category_id',
          to: 'questionnaire_category.id',
        },
      },
      questionnaire: {
        relation: Model.BelongsToOneRelation,
        modelClass: Questionnaire,
        join: {
          from: 'user_questionnaire_answers.questionnaire_id',
          to: 'questionnaire.id',
        },
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'user_questionnaire_answers.user_id',
          to: 'user.id',
        },
      },
      questionnaireAnswer: {
        relation: Model.BelongsToOneRelation,
        modelClass: QuestionnaireAnswer,
        join: {
          from: 'user_questionnaire_answers.questionnaire_answer_id',
          to: 'questionnaire_answer.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = UserQuestionnaireAnswers;
