const { Model } = require('objection');

class MobileAnswerComponent extends Model {
  static get tableName() {
    return 'mobile_answer_component';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        s_id: { type: 'integer' },
        name: { type: 'string' },
        image_id: { type: ['string', 'null'], format: 'uuid' },
        status_id: { type: 'string', format: 'uuid' },
        app_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const Images = require('../Image');
    const EntityStatus = require('../EntityStatus');
    const MobileApp = require('../MobileApp/MobileApp');

    return {
      image: {
        relation: Model.BelongsToOneRelation,
        modelClass: Images,
        join: {
          from: 'mobile_answer_component.image_id',
          to: 'image.id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'mobile_answer_component.status_id',
          to: 'entity_status.id',
        },
      },
      app: {
        relation: Model.BelongsToOneRelation,
        modelClass: MobileApp,
        join: {
          from: 'mobile_answer_component.app_id',
          to: 'mobile_app.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = MobileAnswerComponent;
