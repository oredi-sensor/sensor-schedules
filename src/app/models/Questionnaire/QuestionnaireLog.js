const { Model } = require('objection');

class QuestionnaireLog extends Model {
  static get tableName() {
    return 'questionnaire_log';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['target', 'target_action', 'made_by', 'previous_row'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        target: { type: 'string' },
        target_action: { type: 'string' },
        made_by: { type: 'string', format: 'uuid' },
        previous_row: { type: 'json' },
        new_row: { type: 'json' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const User = require('../User');

    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'questionnaire_log.made_by',
          to: 'user.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = QuestionnaireLog;
