const { Model } = require('objection');

class QuestionTag extends Model {
  static get tableName() {
    return 'question_tag';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['question_id', 'tag_id'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        question_id: { type: 'string', format: 'uuid' },
        tag_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const Question = require('./Question');
    const Tag = require('../Tags/Tag');

    return {
      question: {
        relation: Model.BelongsToOneRelation,
        modelClass: Question,
        join: {
          from: 'question_tag.question_id',
          to: 'question.id',
        },
      },
      tag: {
        relation: Model.BelongsToOneRelation,
        modelClass: Tag,
        join: {
          from: 'question_tag.tag_id',
          to: 'tag.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = QuestionTag;
