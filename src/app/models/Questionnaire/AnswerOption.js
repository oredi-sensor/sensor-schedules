const { Model } = require('objection');

class AnswerOption extends Model {
  static get tableName() {
    return 'answer_option';
  }

  static get idColumn() {
    return ['id'];
  }

  static get modifiers() {
    return {
      notDeleted(builder) {
        builder.leftJoinRelation('entityStatus');
        builder.whereNot('entityStatus.status', 'DELETED');
      },
      orderByIndex(builder) {
        builder.orderBy('question_answer_option.index', 'asc');
      },
    };
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        text_id: { type: ['string', 'null'], format: 'uuid' },
        image_id: { type: ['string', 'null'], format: 'uuid' },
        video_id: { type: ['string', 'null'], format: 'uuid' },
        status_id: { type: 'string', format: 'uuid' },
        answer_type: { type: 'string', minLength: 1, maxLength: 30 },
        index: { type: 'integer' },
        s_id: { type: 'integer' },
        create_by: { type: 'string', format: 'uuid' },
        create_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const Image = require('../Image');
    const Video = require('../Video');
    const StringResources = require('../StringResources');
    const User = require('../User');
    const EntityStatus = require('../EntityStatus');

    return {
      image: {
        relation: Model.BelongsToOneRelation,
        modelClass: Image,
        join: {
          from: 'answer_option.image_id',
          to: 'image.id',
        },
      },
      video: {
        relation: Model.BelongsToOneRelation,
        modelClass: Video,
        join: {
          from: 'answer_option.video_id',
          to: 'video.id',
        },
      },
      stringResources: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'answer_option.text_id',
          to: 'string_resources.id',
        },
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'answer_option.created_by',
          to: 'user.id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'answer_option.status_id',
          to: 'entity_status.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = AnswerOption;
