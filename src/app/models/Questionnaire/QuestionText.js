const { Model } = require('objection');

class QuestionAnswerOption extends Model {
  static get tableName() {
    return 'question_text';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        text_id: { type: 'string', format: 'uuid' },
        question_id: { type: 'string', format: 'uuid' },
      },
    };
  }

  static get relationMappings() {
    const Question = require('./Question');
    const StringResources = require('../StringResources');

    return {
      question: {
        relation: Model.BelongsToOneRelation,
        modelClass: Question,
        join: {
          from: 'question_text.question_id',
          to: 'question.id',
        },
      },
      text: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'question_text.text_id',
          to: 'string_resources.id',
        },
      },
    };
  }
  /*
        $beforeInsert() {
            this.created_at = new Date().toISOString();
        }

        $beforeUpdate() {
            this.updated_at = new Date().toISOString();
        }
        */
}

module.exports = QuestionAnswerOption;
