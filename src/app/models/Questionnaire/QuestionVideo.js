const { Model } = require('objection');

class QuestionVideo extends Model {
  static get tableName() {
    return 'question_video';
  }

  static get idColumn() {
    return ['qv_id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        qv_id: { type: 'string', format: 'uuid' },
        video_id: { type: 'string', format: 'uuid' },
        question_id: { type: 'string', format: 'uuid' },
      },
    };
  }

  static get relationMappings() {
    const Question = require('./Question');
    const Videos = require('../Video');

    return {
      question: {
        relation: Model.BelongsToOneRelation,
        modelClass: Question,
        join: {
          from: 'question_video.question_id',
          to: 'question.id',
        },
      },
      video: {
        relation: Model.BelongsToOneRelation,
        modelClass: Videos,
        // filter: query => query.select('id', 'image_name'),
        join: {
          from: 'question_video.video_id',
          to: 'image.id',
        },
      },
    };
  }
}

module.exports = QuestionVideo;
