const { Model } = require('objection');

class QuestionAnswerOption extends Model {
  static get tableName() {
    return 'question_answer_option';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        answer_option_id: { type: 'string', format: 'uuid' },
        question_id: { type: 'string', format: 'uuid' },
        index: { type: 'integer' },
      },
    };
  }

  static get relationMappings() {
    const Question = require('./Question');
    const AnswerOption = require('./AnswerOption');

    return {
      question: {
        relation: Model.BelongsToOneRelation,
        modelClass: Question,
        join: {
          from: 'question_answer_option.question_id',
          to: 'question.id',
        },
      },
      answerOption: {
        relation: Model.BelongsToOneRelation,
        modelClass: AnswerOption,
        join: {
          from: 'question_answer_option.answer_option_id',
          to: 'answer_option.id',
        },
      },
    };
  }
  /*
    $beforeInsert() {
        this.created_at = new Date().toISOString();
    }

    $beforeUpdate() {
        this.updated_at = new Date().toISOString();
    }
    */
}

module.exports = QuestionAnswerOption;
