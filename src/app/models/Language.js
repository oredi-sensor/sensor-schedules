const { Model } = require('objection');

class Language extends Model {
  static get tableName() {
    return 'language';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        name: { type: 'string' },
        code: { type: 'string' },
      },
    };
  }

  static get relationMappings() {
    const Translations = require('./Translations');

    return {
      translations: {
        relation: Model.HasManyRelation,
        modelClass: Translations,
        join: {
          from: 'language.id',
          to: 'translations.language_id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = Language;
