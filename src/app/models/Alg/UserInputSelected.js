const { Model } = require('objection');

class UserInputSelected extends Model {
  static get tableName() {
    return 'app_user_input_selected';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        user_id: { type: 'string', format: 'uuid' },
        criteria_id: { type: 'string', format: 'uuid' },
        element_id: { type: 'string', format: 'uuid' },
        input_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const AlgCriteria = require('./AlgCriteria');
    const AlgElement = require('./AlgElement');
    const User = require('../User');

    return {
      criteria: {
        relation: Model.BelongsToOneRelation,
        modelClass: AlgCriteria,
        join: {
          from: 'app_user_input_selected.criteria_id',
          to: 'alg_criteria.id',
        },
      },
      element: {
        relation: Model.BelongsToOneRelation,
        modelClass: AlgElement,
        join: {
          from: 'app_user_input_selected.element_id',
          to: 'alg_elements.id',
        },
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'app_user_input_selected.user_id',
          to: 'user.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }
}

module.exports = UserInputSelected;
