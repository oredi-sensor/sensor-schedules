const { Model } = require('objection');

class AlgCriteria extends Model {
  static get tableName() {
    return 'alg_criteria';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        name: { type: 'string', format: 'uuid' },
        meta_skill_id: { type: ['string', 'null'], format: 'uuid' },
        criteria_usage_id: { type: ['string', 'null'], format: 'uuid' },
        principle_id: { type: ['string', 'null'], format: 'uuid' },
        is_relevant: { type: ['boolean', 'null'] },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
        s_id: { type: 'integer' },
        status_id: { type: 'string', format: 'uuid' },
        description: { type: ['string', 'null'] },
      },
    };
  }

  static get relationMappings() {
    const QuestionnaireCategoryQuestionCriteria = require('../Questionnaire/QuestionnaireCategoryQuestionCriteria');

    const AlgMetaSkill = require('./AlgMetaSkill');
    const AlgCriteriaUsage = require('./AlgCriteriaUsage');
    const AlgPrinciple = require('./AlgPrinciple');
    const UserCriteriaApp = require('./UserCriteriaApp');
    const EntityStatus = require('../EntityStatus');
    const StringResources = require('../StringResources');
    const TextConfig = require('../MobileApp/TextConfig');
    const OutputMetaData = require('../MetaData/OutputMetaData');

    return {
      stringResources: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'alg_criteria.name',
          to: 'string_resources.id',
        },
      },
      questionnaireCategoryQuestionCriteria: {
        relation: Model.HasManyRelation,
        modelClass: QuestionnaireCategoryQuestionCriteria,
        join: {
          from: 'alg_criteria.id',
          to: 'questionnaire_category_question_criteria.criteria_id',
        },
      },
      metaSkill: {
        relation: Model.BelongsToOneRelation,
        modelClass: AlgMetaSkill,
        join: {
          from: 'alg_criteria.meta_skill_id',
          to: 'alg_meta_skill.id',
        },
      },
      criteriaUsage: {
        relation: Model.BelongsToOneRelation,
        modelClass: AlgCriteriaUsage,
        join: {
          from: 'alg_criteria.criteria_usage_id',
          to: 'alg_criteria_usage.id',
        },
      },
      principle: {
        relation: Model.BelongsToOneRelation,
        modelClass: AlgPrinciple,
        join: {
          from: 'alg_criteria.principle_id',
          to: 'alg_principle.id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'alg_criteria.status_id',
          to: 'entity_status.id',
        },
      },
      userCriteriaApp: {
        relation: Model.HasManyRelation,
        modelClass: UserCriteriaApp,
        join: {
          from: 'alg_criteria.id',
          to: 'user_criteria_app.criteria_id',
        },
      },
      textConfig: {
        relation: Model.HasManyRelation,
        modelClass: TextConfig,
        join: {
          from: 'alg_criteria.id',
          to: 'text_config.entity_id',
        },
      },
      md: {
        relation: Model.HasManyRelation,
        modelClass: OutputMetaData,
        join: {
          from: 'alg_criteria.id',
          to: 'output_meta_data.criteria_id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = AlgCriteria;
