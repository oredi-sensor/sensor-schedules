const { Model } = require('objection');

class AlgElement extends Model {
  static get tableName() {
    return 'alg_elements';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        name: { type: 'uuid' },
        status_id: { type: 'string', format: 'uuid' },
        principle_id: { type: ['string', 'null'], format: 'uuid' },
        s_id: { type: 'integer' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const AlgPrinciple = require('./AlgPrinciple');
    const StringResources = require('../StringResources');
    const Criteria = require('../Alg/AlgCriteria');
    const Feeling = require('../Alg/Feeling');
    const Organ = require('../Alg/Organ');
    const Output = require('../Output/Output');
    const EntityStatus = require('../EntityStatus');
    const Season = require('../Alg/Season');
    const NaturalElement = require('../Alg/NaturalElement');
    const Character = require('../Alg/Character');

    return {
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'alg_elements.status_id',
          to: 'entity_status.id',
        },
      },
      stringResources: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'alg_elements.name',
          to: 'string_resources.id',
        },
      },
      principle: {
        relation: Model.BelongsToOneRelation,
        modelClass: AlgPrinciple,
        join: {
          from: 'alg_elements.principle_id',
          to: 'alg_principle.id',
        },
      },
      energyGraph: {
        relation: Model.ManyToManyRelation,
        modelClass: AlgElement,
        join: {
          from: 'alg_elements.id',
          through: {
            // persons_movies is the join table.
            from: 'alg_element_element.from',
            to: 'alg_element_element.to',
          },
          to: 'alg_elements.id',
        },
      },
      reverseEnergyGraph: {
        relation: Model.ManyToManyRelation,
        modelClass: AlgElement,
        join: {
          from: 'alg_elements.id',
          through: {
            // persons_movies is the join table.
            from: 'alg_element_element.to',
            to: 'alg_element_element.from',
          },
          to: 'alg_elements.id',
        },
      },
      criteria: {
        relation: Model.ManyToManyRelation,
        modelClass: Criteria,
        join: {
          from: 'alg_elements.id',
          through: {
            from: 'element_attribute.element_id',
            to: 'element_attribute.criteria_id',
          },
          to: 'alg_criteria.id',
        },
      },

      feelings: {
        relation: Model.ManyToManyRelation,
        modelClass: Feeling,
        join: {
          from: 'alg_elements.id',
          through: {
            from: 'element_attribute.element_id',
            to: 'element_attribute.feeling_id',
          },
          to: 'feeling.id',
        },
      },
      organs: {
        relation: Model.ManyToManyRelation,
        modelClass: Organ,
        join: {
          from: 'alg_elements.id',
          through: {
            from: 'element_attribute.element_id',
            to: 'element_attribute.organ_id',
          },
          to: 'organ.id',
        },
      },
      seasons: {
        relation: Model.ManyToManyRelation,
        modelClass: Season,
        join: {
          from: 'alg_elements.id',
          through: {
            from: 'element_attribute.element_id',
            to: 'element_attribute.season_id',
          },
          to: 'season.id',
        },
      },
      naturalElements: {
        relation: Model.ManyToManyRelation,
        modelClass: NaturalElement,
        join: {
          from: 'alg_elements.id',
          through: {
            from: 'element_attribute.element_id',
            to: 'element_attribute.natural_element_id',
          },
          to: 'natural_element.id',
        },
      },
      characters: {
        relation: Model.ManyToManyRelation,
        modelClass: Character,
        join: {
          from: 'alg_elements.id',
          through: {
            from: 'element_attribute.element_id',
            to: 'element_attribute.character_id',
          },
          to: 'character.id',
        },
      },
      outputs: {
        relation: Model.ManyToManyRelation,
        modelClass: Output,
        join: {
          from: 'alg_elements.id',
          through: {
            from: 'element_attribute.element_id',
            to: 'element_attribute.output_id',
          },
          to: 'output.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = AlgElement;
