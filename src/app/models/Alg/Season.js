const { Model } = require('objection');

class Season extends Model {
  static get tableName() {
    return 'season';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        name: { type: 'string', format: 'uuid' },
        s_id: { type: 'integer' },
        status_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const EntityStatus = require('../EntityStatus');
    const StringResources = require('../StringResources');

    return {
      stringResources: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'season.name',
          to: 'string_resources.id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'season.status_id',
          to: 'entity_status.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = Season;
