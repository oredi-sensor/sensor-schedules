const { Model } = require('objection');

class AlgMetaSkill extends Model {
  static get tableName() {
    return 'alg_meta_skill';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        name: { type: 'string', format: 'uuid' },
        code: { type: 'varchar' },
        s_id: { type: 'integer' },
        status_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const AlgCriteria = require('./AlgCriteria');
    const EntityStatus = require('../EntityStatus');
    const StringResources = require('../StringResources');

    return {
      stringResources: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'alg_meta_skill.name',
          to: 'string_resources.id',
        },
      },
      algCriteria: {
        relation: Model.HasManyRelation,
        modelClass: AlgCriteria,
        join: {
          from: 'alg_meta_skill.id',
          to: 'alg_criteria.meta_skill_id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'alg_meta_skill.status_id',
          to: 'entity_status.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = AlgMetaSkill;
