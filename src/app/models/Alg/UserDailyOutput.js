const { Model } = require('objection');

class UserDailyOutput extends Model {
  static get tableName() {
    return 'app_user_daily_output';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        user_id: { type: 'string', format: 'uuid' },
        completed: { type: 'boolean' },
        criteria_id: { type: 'string', format: 'uuid' },
        input_id: { type: 'string', format: 'uuid' },
        selected_input_id: { type: 'string', format: 'uuid' },
        support_element_id: { type: 'string', format: 'uuid' },
        output_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const AlgCriteria = require('./AlgCriteria');
    const AlgElement = require('./AlgElement');
    const UserInputSelected = require('./UserInputSelected');
    const User = require('../User');
    const Output = require('../Output/Output');

    return {
      criteria: {
        relation: Model.BelongsToOneRelation,
        modelClass: AlgCriteria,
        join: {
          from: 'app_user_daily_output.criteria_id',
          to: 'alg_criteria.id',
        },
      },
      inputSelected: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserInputSelected,
        join: {
          from: 'app_user_daily_output.selected_input_id',
          to: 'app_user_input_selected.id',
        },
      },
      supportElement: {
        relation: Model.BelongsToOneRelation,
        modelClass: AlgElement,
        join: {
          from: 'app_user_daily_output.support_element_id',
          to: 'alg_elements.id',
        },
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'app_user_daily_output.user_id',
          to: 'user.id',
        },
      },
      output: {
        relation: Model.BelongsToOneRelation,
        modelClass: Output,
        join: {
          from: 'app_user_daily_output.output_id',
          to: 'output.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = UserDailyOutput;
