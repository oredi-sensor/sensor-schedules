const { Model } = require('objection');

class UserCriteriaApp extends Model {
  static get tableName() {
    return 'user_criteria_app';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        s_id: { type: 'integer' },
        user_id: { type: ['string', 'null'], format: 'uuid' },
        criteria_id: { type: 'string', format: 'uuid' },
        app_id: { type: 'string', format: 'uuid' },
        free: { type: ['boolean', 'null'] },
        auto_renew: { type: ['boolean', 'null'] },
        initial_date: { type: 'timestamp' },
        expire_date: { type: 'timestamp' },
        status_id: { type: 'string', format: 'uuid' },
        is_highlight: { type: 'boolean' },
      },
    };
  }

  static get relationMappings() {
    const AlgCriteria = require('./AlgCriteria');
    const User = require('../User');
    const MobileApp = require('../MobileApp/MobileApp');
    const EntityStatus = require('../EntityStatus');
    const UserCriteriaResultsByPrinciple = require('../Core/UserCriteriaResultsByPrinciple');

    return {
      criteria: {
        relation: Model.BelongsToOneRelation,
        modelClass: AlgCriteria,
        join: {
          from: 'user_criteria_app.criteria_id',
          to: 'alg_criteria.id',
        },
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'user_criteria_app.user_id',
          to: 'user.id',
        },
      },
      app: {
        relation: Model.BelongsToOneRelation,
        modelClass: MobileApp,
        join: {
          from: 'user_criteria_app.app_id',
          to: 'mobile_app.id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'user_criteria_app.status_id',
          to: 'entity_status.id',
        },
      },
      criteriaAnalitycsByPrinciple: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserCriteriaResultsByPrinciple,
        join: {
          from: 'user_criteria_app.criteria_id',
          to: 'user_criteria_results_by_principle.criteria_id',
        },
      },
    };
  }
}

module.exports = UserCriteriaApp;
