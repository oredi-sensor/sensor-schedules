const { Model } = require('objection');

class ElementAttribute extends Model {
  static get tableName() {
    return 'element_attribute';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        element_id: { type: 'string', format: 'uuid' },
        output_id: { type: 'string', format: 'uuid' },
        feeling_id: { type: 'string', format: 'uuid' },
        organ_id: { type: 'string', format: 'uuid' },
        season_id: { type: 'string', format: 'uuid' },
        natural_element_id: { type: 'string', format: 'uuid' },
        character_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const Output = require('../Output/Output');
    const Feeling = require('../Alg/Feeling');
    const Organ = require('../Alg/Organ');
    const Element = require('../Alg/AlgElement');
    const Season = require('../Alg/Season');
    const NaturalElement = require('../Alg/NaturalElement');
    const Character = require('../Alg/Character');

    return {
      element: {
        relation: Model.BelongsToOneRelation,
        modelClass: Element,
        join: {
          from: 'element_attribute.element_id',
          to: 'alg_elements.id',
        },
      },
      output: {
        relation: Model.BelongsToOneRelation,
        modelClass: Output,
        join: {
          from: 'element_attribute.output_id',
          to: 'output.id',
        },
      },

      feeling: {
        relation: Model.BelongsToOneRelation,
        modelClass: Feeling,
        join: {
          from: 'element_attribute.feeling_id',
          to: 'feeling.id',
        },
      },
      organ: {
        relation: Model.BelongsToOneRelation,
        modelClass: Organ,
        join: {
          from: 'element_attribute.organ_id',
          to: 'organ.id',
        },
      },
      season: {
        relation: Model.BelongsToOneRelation,
        modelClass: Season,
        join: {
          from: 'element_attribute.season_id',
          to: 'season.id',
        },
      },
      naturalElement: {
        relation: Model.BelongsToOneRelation,
        modelClass: NaturalElement,
        join: {
          from: 'element_attribute.natural_element_id',
          to: 'natural_element.id',
        },
      },
      character: {
        relation: Model.BelongsToOneRelation,
        modelClass: Character,
        join: {
          from: 'element_attribute.character_id',
          to: 'character.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = ElementAttribute;
