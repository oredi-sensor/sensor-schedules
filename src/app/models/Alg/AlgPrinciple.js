const { Model } = require('objection');

class AlgPrinciple extends Model {
  static get tableName() {
    return 'alg_principle';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        name: { type: 'string', format: 'uuid' },
        code: { type: 'varchar' },
        sense_id: { type: ['string', 'null'], format: 'uuid' },
        s_id: { type: 'integer' },
        status_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const AlgCriteria = require('./AlgCriteria');
    const AlgElement = require('./AlgElement');
    const EntityStatus = require('../EntityStatus');
    const StringResources = require('../StringResources');
    const Sense = require('../Alg/Sense');

    return {
      stringResources: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'alg_principle.name',
          to: 'string_resources.id',
        },
      },
      algCriteria: {
        relation: Model.HasManyRelation,
        modelClass: AlgCriteria,
        join: {
          from: 'alg_principle.id',
          to: 'alg_criteria.principle_id',
        },
      },
      element: {
        relation: Model.HasOneRelation,
        modelClass: AlgElement,
        join: {
          from: 'alg_principle.id',
          to: 'alg_elements.principle_id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'alg_principle.status_id',
          to: 'entity_status.id',
        },
      },
      sense: {
        relation: Model.BelongsToOneRelation,
        modelClass: Sense,
        join: {
          from: 'alg_principle.sense_id',
          to: 'sense.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = AlgPrinciple;
