const { Model } = require('objection');

class UserQuestionnaireAnalitycsResults extends Model {
  static get tableName() {
    return 'user_criteria_results';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        questionnaire_id: { type: 'string', format: 'uuid' },
        user_id: { type: 'string', format: 'uuid' },
        name: { type: 'string', format: 'uuid' },
        code: { type: 'string' },
        meta_skill: { type: 'string', format: 'uuid' },
        principle: { type: 'string', format: 'uuid' },
        number: { type: 'number' },
        strength: { type: 'number' },
        score: { type: 'number' },
        is_relevant: { type: 'boolean' },
        reactivity: { type: 'number' },
      },
    };
  }
}

module.exports = UserQuestionnaireAnalitycsResults;
