const { Model } = require('objection');

class UserCriteriaResultsByPrinciple extends Model {
  static get tableName() {
    return 'user_criteria_results_by_principle';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        questionnaire_id: { type: 'string', format: 'uuid' },
        user_id: { type: 'string', format: 'uuid' },
        name: { type: 'string', format: 'uuid' },
        criteria_id: { type: 'string', format: 'uuid' },
        criteria_usage_id: { type: 'string', format: 'uuid' },
        code: { type: 'string' },
        meta_skill: { type: 'string', format: 'uuid' },
        principle: { type: 'string', format: 'uuid' },
        number: { type: 'number' },
        strength: { type: 'number' },
        score: { type: 'number' },
        is_relevant: { type: 'boolean' },
        reactivity: { type: 'number' },
        avg_reactivity: { type: 'number' },
        delta: { type: 'number' },
      },
    };
  }

  static get relationMappings() {
    const AlgPrinciple = require('../Alg/AlgPrinciple');
    const AlgCriteria = require('../Alg/AlgCriteria');

    return {
      algPrinciple: {
        relation: Model.BelongsToOneRelation,
        modelClass: AlgPrinciple,
        join: {
          from: 'user_criteria_results_by_principle.principle',
          to: 'alg_principle.id',
        },
      },
      algCriteria: {
        relation: Model.BelongsToOneRelation,
        modelClass: AlgCriteria,
        join: {
          from: 'user_criteria_results_by_principle.criteria_id',
          to: 'alg_criteria.id',
        },
      },
    };
  }
}

module.exports = UserCriteriaResultsByPrinciple;
