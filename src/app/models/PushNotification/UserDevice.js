const { Model } = require('objection');

class UserDevice extends Model {
  static get tableName() {
    return 'pn_user_device';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['user_id', 'fcm_token'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        user_id: { type: 'string', format: 'uuid' },
        fcm_token: { type: 'string', minLength: 1, maxLength: 255 },
        active: { type: 'boolean' },
      },
    };
  }

  static get relationMappings() {
    const User = require('../User');

    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'pn_user_device.user_id',
          to: 'user.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = UserDevice;
