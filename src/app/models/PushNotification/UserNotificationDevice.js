const { Model } = require('objection');

class UserNotificationDevice extends Model {
  static get tableName() {
    return 'pn_user_notification_device';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [
        'notification_id',
        'push_notification_id',
        'user_id',
        'device_id',
      ],
      properties: {
        id: { type: 'string', format: 'uuid' },
        notification_id: { type: 'string', format: 'uuid' },
        push_notification_id: { type: 'string', minLength: 1, maxLength: 255 },
        user_id: { type: 'string', format: 'uuid' },
        device_id: { type: 'string', format: 'uuid' },
        delivered: { type: 'boolean' },
        read_at: { type: 'date' },
      },
    };
  }

  static get relationMappings() {
    const User = require('../User');
    const Notification = require('./Notification');
    const UserDevice = require('./UserDevice');

    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'pn_user_notification_device.user_id',
          to: 'user.id',
        },
      },
      notification: {
        relation: Model.BelongsToOneRelation,
        modelClass: Notification,
        join: {
          from: 'pn_user_notification_device.notification_id',
          to: 'pn_notification.id',
        },
      },
      userDevice: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserDevice,
        join: {
          from: 'pn_user_notification_device.device_id',
          to: 'pn_user_device.id',
        },
      },
    };
  }
}

module.exports = UserNotificationDevice;
