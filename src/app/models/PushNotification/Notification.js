const { Model } = require('objection');

class Notification extends Model {
  static get tableName() {
    return 'pn_notification';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['title', 'message'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        title: { type: 'string', minLength: 1, maxLength: 255 },
        message: { type: 'string', minLength: 1, maxLength: 255 },
        mobile_app_id: { type: ['string', 'null'], format: 'uuid' },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }
}

module.exports = Notification;
