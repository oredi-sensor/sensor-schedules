const { Model } = require('objection');

class UserNotification extends Model {
  static get tableName() {
    return 'pn_user_notification';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['notification_id', 'user_id'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        notification_id: { type: 'string', format: 'uuid' },
        user_id: { type: 'string', format: 'uuid' },
        read_by: { type: 'string', format: 'uuid' },
        read_at: { type: 'date' },
      },
    };
  }

  static get relationMappings() {
    const User = require('../User');
    const Notification = require('./Notification');
    const UserDevice = require('./UserDevice');

    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'pn_user_notification.user_id',
          to: 'user.id',
        },
      },
      notification: {
        relation: Model.BelongsToOneRelation,
        modelClass: Notification,
        join: {
          from: 'pn_user_notification.notification_id',
          to: 'pn_notification.id',
        },
      },
      device: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserDevice,
        join: {
          from: 'pn_user_notification.read_by',
          to: 'pn_user_device.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = UserNotification;
