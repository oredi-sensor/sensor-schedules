const { Model } = require('objection');

class StringResources extends Model {
  static get tableName() {
    return 'string_resources';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        name: { type: 'string' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const Translations = require('./Translations');
    const Tag = require('./Tags/Tag');
    const Questionnaire = require('./Questionnaire/Questionnaire');
    const QuestionnaireCategory = require('./Questionnaire/QuestionnaireCategory');

    return {
      translations: {
        relation: Model.HasManyRelation,
        modelClass: Translations,
        join: {
          from: 'string_resources.id',
          to: 'translations.string_resource_id',
        },
      },
      tags: {
        relation: Model.HasManyRelation,
        modelClass: Tag,
        join: {
          from: 'string_resources.id',
          to: 'tag.string_resource_id',
        },
      },
      questionnaires: {
        relation: Model.HasManyRelation,
        modelClass: Questionnaire,
        join: {
          from: 'string_resources.id',
          to: 'questionnaire.name',
        },
      },
      questionnaireCategories: {
        relation: Model.HasManyRelation,
        modelClass: QuestionnaireCategory,
        join: {
          from: 'string_resources.id',
          to: 'questionnaire_category.name',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = StringResources;
