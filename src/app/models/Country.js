const { Model } = require('objection');

class Country extends Model {
  static get tableName() {
    return 'country';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name', 'code'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        code: { type: 'string', minLength: 1, maxLength: 40 },
        name: { type: 'string', format: 'uuid' },
      },
    };
  }

  static get relationMappings() {
    const Company = require('./Company');
    const StringResources = require('./StringResources');

    return {
      collaborators: {
        relation: Model.HasManyRelation,
        modelClass: Company,
        join: {
          from: 'country.id',
          to: 'company.company_nif',
        },
      },
      stringResources: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'country.name',
          to: 'string_resources.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = Country;
