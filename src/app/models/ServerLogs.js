const { Model } = require('objection');

class ServerLogs extends Model {
  static get tableName() {
    return 'server_logs';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: ['string', null], format: 'uuid' },
        user_id: { type: ['string', null], format: 'uuid' },
        remote_address: { type: ['string', null] },
        method: { type: ['string', null] },
        path: { type: ['string', null] },
        status_code: { type: ['string', null] },
        log_data: { type: 'json' },
        created_at: { type: 'timestamp' },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = ServerLogs;
