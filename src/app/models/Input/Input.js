const { Model } = require('objection');

class Input extends Model {
  static get tableName() {
    return 'input';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        name: { type: 'string' },
        image_id: { type: 'string', format: 'uuid' },
        status_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const EntityStatus = require('../EntityStatus');
    const StringResources = require('../StringResources');
    const Image = require('../Image');
    const Season = require('../Alg/Season');
    const NaturalElement = require('../Alg/NaturalElement');
    const Criteria = require('../Alg/AlgCriteria');

    return {
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'input.status_id',
          to: 'entity_status.id',
        },
      },
      image: {
        relation: Model.BelongsToOneRelation,
        modelClass: Image,
        join: {
          from: 'input.image_id',
          to: 'image.id',
        },
      },
      criteria: {
        relation: Model.ManyToManyRelation,
        modelClass: Criteria,
        join: {
          from: 'input.id',
          through: {
            from: 'image_meta_data.input_id',
            to: 'image_meta_data.criteria_id',
          },
          to: 'alg_criteria.id',
        },
      },
      seasons: {
        relation: Model.ManyToManyRelation,
        modelClass: Season,
        join: {
          from: 'input.id',
          through: {
            from: 'image_meta_data.input_id',
            to: 'image_meta_data.season_id',
          },
          to: 'season.id',
        },
      },
      naturalElements: {
        relation: Model.ManyToManyRelation,
        modelClass: NaturalElement,
        join: {
          from: 'input.id',
          through: {
            from: 'image_meta_data.input_id',
            to: 'image_meta_data.natural_element_id',
          },
          to: 'natural_element.id',
        },
      },
      nameInput: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'input.name',
          to: 'string_resources.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = Input;
