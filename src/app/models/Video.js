const { Model } = require('objection');

class Video extends Model {
  static get tableName() {
    return 'video';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['video_name', 'video_url', 'poster_name', 'poster_url'],
      properties: {
        id: { type: 'string', format: 'uuid' },
        video_name: { type: 'string' },
        video_url: { type: 'string', maxLength: 350 },
        poster_name: { type: 'string' },
        poster_url: { type: 'string', maxLength: 350 },
        entity_status_id: { type: 'string', format: 'uuid' },
      },
    };
  }

  static get relationMappings() {
    const EntityStatus = require('./EntityStatus');

    return {
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'video.entity_status_id',
          to: 'entity_status.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = Video;
