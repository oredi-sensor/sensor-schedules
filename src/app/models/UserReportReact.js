const { Model } = require('objection');

class UserReportReact extends Model {
  static get tableName() {
    return 'app_user_react_report';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        report: { type: 'string' },
        audio_file_name: { type: 'string' },
        audio_duration: { type: 'string' },
        user_id: { type: 'string', format: 'uuid' },
        device_id: { type: 'string', format: 'uuid' },
        language_id: { type: 'string', format: 'uuid' },
        is_voice_recorder: { type: 'boolean' },
        is_highlight: { type: 'boolean' },
        created_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const User = require('./User');
    const Language = require('./Language');
    const Device = require('./PushNotification/UserDevice');

    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'app_user_react_report.user_id',
          to: 'user.id',
        },
      },
      device: {
        relation: Model.BelongsToOneRelation,
        modelClass: Device,
        join: {
          from: 'app_user_react_report.device_id',
          to: 'pn_user_device.id',
        },
      },
      language: {
        relation: Model.BelongsToOneRelation,
        modelClass: Language,
        join: {
          from: 'app_user_react_report.language_id',
          to: 'language.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }
}

module.exports = UserReportReact;
