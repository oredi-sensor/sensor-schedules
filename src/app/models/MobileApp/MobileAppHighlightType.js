const { Model } = require('objection');

class MobileAppHighlightType extends Model {
  static get tableName() {
    return 'mobile_app_highlight_type';
  }

  static get idColumn() {
    return ['highlight_type'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        highlight_type: { type: 'string' },
        mobile_app_id: { type: 'string', format: 'uuid' },
        status_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const App = require('./MobileApp');
    const EntityStatus = require('../EntityStatus');

    return {
      app: {
        relation: Model.BelongsToOneRelation,
        modelClass: App,
        join: {
          from: 'mobile_app_highlight_type.mobile_app_id',
          to: 'mobile_app.id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'mobile_app_highlight_type.status_id',
          to: 'entity_status.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }
}

module.exports = MobileAppHighlightType;
