const { Model } = require('objection');

class MobileAppQuestionnaire extends Model {
  static get tableName() {
    return 'mobile_app_questionnaire';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        mobile_app_id: { type: 'string', format: 'uuid' },
        status_id: { type: 'string', format: 'uuid' },
        questionnaire_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const App = require('./MobileApp');
    const EntityStatus = require('../EntityStatus');
    const Questionnaire = require('../Questionnaire/Questionnaire');
    const HighlightType = require('./MobileAppHighlightType');

    return {
      app: {
        relation: Model.BelongsToOneRelation,
        modelClass: App,
        join: {
          from: 'mobile_app_questionnaire.mobile_app_id',
          to: 'mobile_app.id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'mobile_app_questionnaire.status_id',
          to: 'entity_status.id',
        },
      },
      questionnaire: {
        relation: Model.BelongsToOneRelation,
        modelClass: Questionnaire,
        join: {
          from: 'mobile_app_questionnaire.questionnaire_id',
          to: 'questionnaire.id',
        },
      },
      highlightTypes: {
        relation: Model.ManyToManyRelation,
        modelClass: HighlightType,
        join: {
          from: 'mobile_app_questionnaire.id',
          through: {
            from:
              'mobile_app_highlight_type_questionnaire.mobile_app_questionnaire_id',
            to:
              'mobile_app_highlight_type_questionnaire.mobile_app_highlight_type',
          },
          to: 'mobile_app_highlight_type.highlight_type',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = MobileAppQuestionnaire;
