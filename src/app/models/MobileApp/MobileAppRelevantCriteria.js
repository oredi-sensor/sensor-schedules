const { Model } = require('objection');

class MobileAppRelevantCriteria extends Model {
  static get tableName() {
    return 'mobile_app_relevant_criteria';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        mobile_app_id: { type: 'string', format: 'uuid' },
        criteria_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const App = require('./MobileApp');
    const Criteria = require('../Alg/AlgCriteria');

    return {
      app: {
        relation: Model.BelongsToOneRelation,
        modelClass: App,
        join: {
          from: 'mobile_app_relevant_criteria.mobile_app_id',
          to: 'mobile_app.id',
        },
      },

      criteria: {
        relation: Model.BelongsToOneRelation,
        modelClass: Criteria,
        join: {
          from: 'mobile_app_relevant_criteria.criteria_id',
          to: 'alg_criteria.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }
}

module.exports = MobileAppRelevantCriteria;
