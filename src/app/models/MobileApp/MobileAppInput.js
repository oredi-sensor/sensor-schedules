const { Model } = require('objection');

class MobileAppInput extends Model {
  static get tableName() {
    return 'mobile_app_input';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        mobile_app_id: { type: 'string', format: 'uuid' },
        status_id: { type: 'string', format: 'uuid' },
        input_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const App = require('./MobileApp');
    const EntityStatus = require('../EntityStatus');
    const Input = require('../Input/Input');

    return {
      app: {
        relation: Model.BelongsToOneRelation,
        modelClass: App,
        join: {
          from: 'mobile_app_input.mobile_app_id',
          to: 'mobile_app.id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'mobile_app_input.status_id',
          to: 'entity_status.id',
        },
      },
      input: {
        relation: Model.BelongsToOneRelation,
        modelClass: Input,
        join: {
          from: 'mobile_app_input.input_id',
          to: 'input.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = MobileAppInput;
