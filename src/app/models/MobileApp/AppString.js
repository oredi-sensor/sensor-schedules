const { Model } = require('objection');

class AppString extends Model {
  static get tableName() {
    return 'app_string';
  }

  static get idColumn() {
    return 'namespace';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        namespace: { type: 'string' },
        mobile_app_id: { type: 'string', format: 'uuid' },
        status_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const App = require('./MobileApp');
    const EntityStatus = require('../EntityStatus');
    const StaticString = require('../MobileApp/StaticString');
    const RichText = require('../MobileApp/RichText');

    return {
      app: {
        relation: Model.BelongsToOneRelation,
        modelClass: App,
        join: {
          from: 'app_string.app_id',
          to: 'mobile_app.id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'app_string.status_id',
          to: 'entity_status.id',
        },
      },
      staticStrings: {
        relation: Model.HasManyRelation,
        modelClass: StaticString,
        join: {
          from: 'app_string.namespace',
          to: 'static_string.namespace',
        },
      },
      richTexts: {
        relation: Model.HasManyRelation,
        modelClass: RichText,
        join: {
          from: 'app_string.namespace',
          to: 'rich_text.namespace',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = AppString;
