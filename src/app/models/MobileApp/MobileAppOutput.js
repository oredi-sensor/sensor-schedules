const { Model } = require('objection');

class MobileAppOutput extends Model {
  static get tableName() {
    return 'mobile_app_output';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        mobile_app_id: { type: 'string', format: 'uuid' },
        status_id: { type: 'string', format: 'uuid' },
        output_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const App = require('./MobileApp');
    const EntityStatus = require('../EntityStatus');
    const Output = require('../Output/Output');
    const HighlightType = require('./MobileAppHighlightType');

    return {
      app: {
        relation: Model.BelongsToOneRelation,
        modelClass: App,
        join: {
          from: 'mobile_app_output.mobile_app_id',
          to: 'mobile_app.id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'mobile_app_output.status_id',
          to: 'entity_status.id',
        },
      },
      output: {
        relation: Model.BelongsToOneRelation,
        modelClass: Output,
        join: {
          from: 'mobile_app_output.output_id',
          to: 'output.id',
        },
      },
      highlightTypes: {
        relation: Model.ManyToManyRelation,
        modelClass: HighlightType,
        join: {
          from: 'mobile_app_output.id',
          through: {
            from: 'mobile_app_highlight_type_output.mobile_app_output_id',
            to: 'mobile_app_highlight_type_output.mobile_app_highlight_type',
          },
          to: 'mobile_app_highlight_type.highlight_type',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = MobileAppOutput;
