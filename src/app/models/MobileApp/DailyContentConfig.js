const { Model } = require('objection');

class DailyContentConfig extends Model {
  static get tableName() {
    return 'daily_content_config';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        highlight: { type: 'string' },
        day_of_week: { type: 'integer' },
        week_index: { type: 'integer' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = DailyContentConfig;
