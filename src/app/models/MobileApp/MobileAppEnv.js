const { Model } = require('objection');

class MobileAppEnv extends Model {
  static get tableName() {
    return 'mobile_app_env';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        env_name: { type: 'string' },
        env_value: { type: ['string', 'null'] },
        app_id: { type: 'string', format: 'uuid' },
        status_id: { type: 'string', format: 'uuid' },
        entity_name: { type: ['string', 'null'] },
        entity_id: { type: ['string', 'null'], format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const App = require('./MobileApp');
    const EntityStatus = require('../EntityStatus');

    return {
      app: {
        relation: Model.BelongsToOneRelation,
        modelClass: App,
        join: {
          from: 'mobile_app_env.app_id',
          to: 'mobile_app.id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'mobile_app_env.status_id',
          to: 'entity_status.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = MobileAppEnv;
