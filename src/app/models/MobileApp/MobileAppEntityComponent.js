const { Model } = require('objection');

class MobileAppEntityComponent extends Model {
  static get tableName() {
    return 'mobile_app_entity_component';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        entity_name: { type: 'string' },
        app_id: { type: 'string', format: 'uuid' },
        component_id: { type: 'string', format: 'uuid' },
        status_id: { type: 'string', format: 'uuid' },
        entity_id: { type: ['string', 'null'], format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const App = require('./MobileApp');
    const EntityStatus = require('../EntityStatus');
    const Component = require('../Questionnaire/MobileAnswerComponent');

    return {
      app: {
        relation: Model.BelongsToOneRelation,
        modelClass: App,
        join: {
          from: 'mobile_app_entity_component.app_id',
          to: 'mobile_app.id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'mobile_app_entity_component.status_id',
          to: 'entity_status.id',
        },
      },

      component: {
        relation: Model.BelongsToOneRelation,
        modelClass: Component,
        join: {
          from: 'mobile_app_entity_component.component_id',
          to: 'mobile_answer_component.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = MobileAppEntityComponent;
