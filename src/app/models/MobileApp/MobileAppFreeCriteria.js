const { Model } = require('objection');

class MobileAppFreeCriteria extends Model {
  static get tableName() {
    return 'mobile_app_free_criteria';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        mobile_app_id: { type: 'string', format: 'uuid' },
        criteria_id: { type: 'string', format: 'uuid' },
        status_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const App = require('./MobileApp');
    const EntityStatus = require('../EntityStatus');
    const Criteria = require('../Alg/AlgCriteria');

    return {
      app: {
        relation: Model.BelongsToOneRelation,
        modelClass: App,
        join: {
          from: 'mobile_app_free_criteria.mobile_app_id',
          to: 'mobile_app.id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'mobile_app_free_criteria.status_id',
          to: 'entity_status.id',
        },
      },

      criteria: {
        relation: Model.BelongsToOneRelation,
        modelClass: Criteria,
        join: {
          from: 'mobile_app_free_criteria.criteria_id',
          to: 'mobile_answer_component.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = MobileAppFreeCriteria;
