const { Model } = require('objection');

class MobileAppUserOutput extends Model {
  static get tableName() {
    return 'mobile_app_user_output';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        mobile_app_id: { type: 'string', format: 'uuid' },
        user_id: { type: 'string', format: 'uuid' },
        output_id: { type: 'string', format: 'uuid' },
        highlight_type: { type: ' string' },
        created_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const App = require('./MobileApp');
    const EntityStatus = require('../User');
    const Output = require('../Output/Output');
    const HighlightType = require('./MobileAppHighlightType');

    return {
      app: {
        relation: Model.BelongsToOneRelation,
        modelClass: App,
        join: {
          from: 'mobile_app_user_output.app_id',
          to: 'mobile_app.id',
        },
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'mobile_app_user_output.user_id',
          to: 'user.id',
        },
      },
      output: {
        relation: Model.BelongsToOneRelation,
        modelClass: Output,
        join: {
          from: 'mobile_app_user_output.output_id',
          to: 'output.id',
        },
      },
      highlightType: {
        relation: Model.BelongsToOneRelation,
        modelClass: HighlightType,
        join: {
          from: 'mobile_app_user_output.highlight_type',
          to: 'mobile_app_highlight_type.highlight_type',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }
}

module.exports = MobileAppUserOutput;
