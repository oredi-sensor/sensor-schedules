const { Model } = require('objection');

class DailyChallengeConfig extends Model {
  static get tableName() {
    return 'daily_challenge_config';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        mobile_app_id: { type: 'string', format: 'uuid' },
        max_number_inputs: { type: 'integer' },
        min_days_interval: { type: 'integer' },
        highlights: { type: 'object' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const App = require('./MobileApp');

    return {
      app: {
        relation: Model.BelongsToOneRelation,
        modelClass: App,
        join: {
          from: 'daily_challenge_config.mobile_app_id',
          to: 'mobile_app.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = DailyChallengeConfig;
