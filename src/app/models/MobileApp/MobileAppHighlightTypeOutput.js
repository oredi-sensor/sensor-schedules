const { Model } = require('objection');

class MobileAppHighlightTypeOutput extends Model {
  static get tableName() {
    return 'mobile_app_highlight_type_output';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        mobile_app_highlight_type: { type: 'string' },
        status_id: { type: 'string', format: 'uuid' },
        output_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const EntityStatus = require('../EntityStatus');
    const MobileAppOutput = require('./MobileAppOutput');
    const MobileAppHighlight = require('./MobileAppHighlightType');

    return {
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'mobile_app_highlight_type_output.status_id',
          to: 'entity_status.id',
        },
      },
      mobileAppOutput: {
        relation: Model.BelongsToOneRelation,
        modelClass: MobileAppOutput,
        join: {
          from: 'mobile_app_highlight_type_output.mobile_app_output_id',
          to: 'output.id',
        },
      },
      highlightType: {
        relation: Model.BelongsToOneRelation,
        modelClass: MobileAppHighlight,
        join: {
          from: 'mobile_app_highlight_type_output.mobile_app_highlight_type',
          to: 'mobile_app_highlight_type.highlight_type',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }
}

module.exports = MobileAppHighlightTypeOutput;
