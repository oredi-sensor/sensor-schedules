const { Model } = require('objection');

class MobileAppHighlightTypeQuestionnaire extends Model {
  static get tableName() {
    return 'mobile_app_highlight_type_questionnaire';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        mobile_app_highlight_type: { type: 'string' },
        status_id: { type: 'string', format: 'uuid' },
        mobile_app_questionnaire_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const EntityStatus = require('../EntityStatus');
    const MobileAppQuestionnaire = require('./MobileAppQuestionnaire');
    const MobileAppHighlight = require('./MobileAppHighlightType');

    return {
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'mobile_app_highlight_type_questionnaire.status_id',
          to: 'entity_status.id',
        },
      },
      mobileAppQuestionnaire: {
        relation: Model.BelongsToOneRelation,
        modelClass: MobileAppQuestionnaire,
        join: {
          from:
            'mobile_app_highlight_type_questionnaire.mobile_app_questionnaire_id',
          to: 'questionnaire.id',
        },
      },
      highlightType: {
        relation: Model.BelongsToOneRelation,
        modelClass: MobileAppHighlight,
        join: {
          from:
            'mobile_app_highlight_type_questionnaire.mobile_app_highlight_type',
          to: 'mobile_app_highlight_type.highlight_type',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }
}

module.exports = MobileAppHighlightTypeQuestionnaire;
