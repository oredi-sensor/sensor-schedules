const { Model } = require('objection');

class MobileApp extends Model {
  static get tableName() {
    return 'mobile_app';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        name: { type: 'string' },
        public_token: { type: 'string' },
        description: { type: ['string', 'null'] },
        icon: { type: ['string', 'null'], format: 'uuid' },
        published_android: { type: ['boolean', 'null'] },
        published_ios: { type: ['boolean', 'null'] },
        link_android: { type: ['string', 'null'] },
        link_ios: { type: ['string', 'null'] },
        link_appetize_io: { type: ['string', 'null'] },
        status_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const Image = require('../Image');
    const EntityStatus = require('../EntityStatus');
    const User = require('../User');

    return {
      AppIcon: {
        relation: Model.BelongsToOneRelation,
        modelClass: Image,
        join: {
          from: 'mobile_app.icon',
          to: 'image.id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'mobile_app.status_id',
          to: 'entity_status.id',
        },
      },
      users: {
        relation: Model.ManyToManyRelation,
        modelClass: User,
        join: {
          from: 'mobile_app.id',
          through: {
            from: 'mobile_app_user.app_id',
            to: 'mobile_app_user.user_id',
          },
          to: 'user.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = MobileApp;
