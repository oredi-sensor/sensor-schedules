const { Model } = require('objection');

class TextConfig extends Model {
  static get tableName() {
    return 'text_config';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        name: { type: 'string' },
        alias: { type: 'string', format: 'uuid' },
        description: { type: ['string', 'null'], format: 'uuid' },
        entity_name: { type: ['string', 'null'] },
        entity_id: { type: 'string', format: 'uuid' },
        is_active: { type: ['boolean', 'null'] },
        app_id: { type: 'string', format: 'uuid' },
      },
    };
  }

  static get relationMappings() {
    const App = require('./MobileApp');
    const StringResources = require('../StringResources');

    return {
      app: {
        relation: Model.BelongsToOneRelation,
        modelClass: App,
        join: {
          from: 'text_config.app_id',
          to: 'mobile_app.id',
        },
      },
      stringResourcesAlias: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'text_config.alias',
          to: 'string_resources.id',
        },
      },
      stringResourcesDescription: {
        relation: Model.BelongsToOneRelation,
        modelClass: StringResources,
        join: {
          from: 'text_config.description',
          to: 'string_resources.id',
        },
      },
    };
  }
}

module.exports = TextConfig;
