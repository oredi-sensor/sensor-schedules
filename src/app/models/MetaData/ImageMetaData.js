const { Model } = require('objection');

class ImageMetaData extends Model {
  static get tableName() {
    return 'image_meta_data';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        input_id: { type: 'string', format: 'uuid' },
        criteria_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const Image = require('../Image');
    const Criteria = require('../Alg/AlgCriteria');
    const Season = require('../Alg/Season');
    const NaturalElement = require('../Alg/NaturalElement');

    return {
      naturalElement: {
        relation: Model.BelongsToOneRelation,
        modelClass: NaturalElement,
        join: {
          from: 'image_meta_data.natural_element_id',
          to: 'natural_element.id',
        },
      },
      season: {
        relation: Model.BelongsToOneRelation,
        modelClass: Season,
        join: {
          from: 'image_meta_data.season_id',
          to: 'season.id',
        },
      },
      criteria: {
        relation: Model.BelongsToOneRelation,
        modelClass: Criteria,
        join: {
          from: 'image_meta_data.criteria_id',
          to: 'alg_criteria.id',
        },
      },
      input: {
        relation: Model.BelongsToOneRelation,
        modelClass: Image,
        join: {
          from: 'image_meta_data.input_id',
          to: 'image.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = ImageMetaData;
