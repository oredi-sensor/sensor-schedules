const { Model } = require('objection');

class OutputMetaData extends Model {
  static get tableName() {
    return 'output_meta_data';
  }

  static get idColumn() {
    return ['id'];
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [],
      properties: {
        id: { type: 'string', format: 'uuid' },
        output_id: { type: 'string', format: 'uuid' },
        criteria_id: { type: 'string', format: 'uuid' },
        meta_skill_id: { type: 'string', format: 'uuid' },
        principle_id: { type: 'string', format: 'uuid' },
        feeling_id: { type: 'string', format: 'uuid' },
        organ_id: { type: 'string', format: 'uuid' },
        created_at: { type: 'timestamp' },
        updated_at: { type: 'timestamp' },
      },
    };
  }

  static get relationMappings() {
    const Output = require('../Output/Output');
    const Criteria = require('../Alg/AlgCriteria');
    const MetaSkill = require('../Alg/AlgMetaSkill');
    const Principle = require('../Alg/AlgPrinciple');
    const Feeling = require('../Alg/Feeling');
    const Organ = require('../Alg/Organ');
    const Season = require('../Alg/Season');
    const NaturalElement = require('../Alg/NaturalElement');

    return {
      naturalElement: {
        relation: Model.BelongsToOneRelation,
        modelClass: NaturalElement,
        join: {
          from: 'output_meta_data.natural_element__id',
          to: 'natural_element.id',
        },
      },
      season: {
        relation: Model.BelongsToOneRelation,
        modelClass: Season,
        join: {
          from: 'output_meta_data.season_id',
          to: 'season.id',
        },
      },
      output: {
        relation: Model.BelongsToOneRelation,
        modelClass: Output,
        join: {
          from: 'output_meta_data.output_id',
          to: 'output.id',
        },
      },
      criteria: {
        relation: Model.BelongsToOneRelation,
        modelClass: Criteria,
        join: {
          from: 'output_meta_data.criteria_id',
          to: 'alg_criteria.id',
        },
      },
      feeling: {
        relation: Model.BelongsToOneRelation,
        modelClass: Feeling,
        join: {
          from: 'output_meta_data.feeling_id',
          to: 'feeling.id',
        },
      },
      metaSkill: {
        relation: Model.BelongsToOneRelation,
        modelClass: MetaSkill,
        join: {
          from: 'output_meta_data.meta_skill_id',
          to: 'alg_meta_skill.id',
        },
      },
      principle: {
        relation: Model.BelongsToOneRelation,
        modelClass: Principle,
        join: {
          from: 'output_meta_data.principle_id',
          to: 'alg_principle.id',
        },
      },
      organ: {
        relation: Model.BelongsToOneRelation,
        modelClass: Organ,
        join: {
          from: 'output_meta_data.organ_id',
          to: 'organ.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = OutputMetaData;
