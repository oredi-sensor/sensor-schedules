const { Model } = require('objection');

// to encrypt the password
const Password = require('objection-password')();

class User extends Password(Model) {
  static get tableName() {
    return 'user';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      // required: ['email', 'password',],
      properties: {
        id: { type: 'string', format: 'uuid' },
        email: { type: 'string' },
        company_nif: { type: ['integer', 'null'] },
        is_admin: { type: 'boolean' },
        first_name: { type: 'string' },
        last_name: { type: 'string' },
        job_name: { type: ['string', 'null'] },
        birth_place: { type: ['string', 'null'] },
        nationality: { type: ['string', 'null'] },
        nationality_id: { type: ['string', 'null'], format: 'uuid' },
        gender: { type: 'string' },
        birth_date: { type: 'date' },
        status_id: { type: 'string', format: 'uuid' },
        role_id: { type: ['string', 'null'], format: 'uuid' },
        created_at: { type: 'timestamp' },
        token: { type: ['string', 'null'] },
      },
    };
  }

  static get relationMappings() {
    const Token = require('./Token');
    const UserRole = require('./UserRole');
    const Company = require('./Company');
    const Subscription = require('./Subscription/Subscription');
    const EntityStatus = require('./EntityStatus');
    const App = require('./MobileApp/MobileApp');
    const UserDevice = require('./PushNotification/UserDevice');
    const QuestionnaireAnswers = require('./Questionnaire/QuestionnaireAnswer');
    const UserDailyContent = require('./MobileApp/UserDailyContent');

    return {
      dailyContents: {
        relation: Model.HasManyRelation,
        modelClass: UserDailyContent,
        join: {
          from: 'user.id',
          to: 'user_daily_content.user_id',
        },
      },
      questionnaireAnswers: {
        relation: Model.HasManyRelation,
        modelClass: QuestionnaireAnswers,
        join: {
          from: 'user.id',
          to: 'questionnaire_answer.user_id',
        },
      },
      userDevices: {
        relation: Model.HasManyRelation,
        modelClass: UserDevice,
        join: {
          from: 'user.id',
          to: 'pn_user_device.user_id',
        },
      },
      tokens: {
        relation: Model.HasManyRelation,
        modelClass: Token,
        join: {
          from: 'user.id',
          to: 'token.subject_id',
        },
      },
      company: {
        relation: Model.BelongsToOneRelation,
        modelClass: Company,
        join: {
          from: 'user.company_nif',
          to: 'company.company_nif',
        },
      },
      role: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserRole,
        join: {
          from: 'user.role_id',
          to: 'user_role.id',
        },
      },
      subscriptions: {
        relation: Model.HasManyRelation,
        modelClass: Subscription,
        join: {
          from: 'user.id',
          to: 'subscription.user_id',
        },
      },
      mobileApps: {
        relation: Model.ManyToManyRelation,
        modelClass: App,
        join: {
          from: 'user.id',
          through: {
            from: 'mobile_app_user.user_id',
            to: 'mobile_app_user.app_id',
          },
          to: 'mobile_app.id',
        },
      },
      entityStatus: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'user.status_id',
          to: 'entity_status.id',
        },
      },
      nationalityId: {
        relation: Model.BelongsToOneRelation,
        modelClass: EntityStatus,
        join: {
          from: 'user.nationality_id',
          to: 'country.id',
        },
      },
    };
  }

  async $beforeInsert(queryContext) {
    await super.$beforeInsert(queryContext);
    this.created_at = new Date().toISOString();
  }
}

module.exports = User;
