const { Model } = require('objection');

class Company extends Model {
  static get tableName() {
    return 'company';
  }

  static get idColumn() {
    return 'company_nif';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [
        'name',
        'locale',
        'postcode',
        'billing_address',
        'company_nif',
        'country_id',
      ],
      properties: {
        company_nif: { type: 'integer' },
        locale: { type: 'string', minLength: 1, maxLength: 30 },
        postcode: { type: 'string', minLength: 1, maxLength: 30 },
        name: { type: 'string', minLength: 1, maxLength: 255 },
        billing_address: { type: 'string', minLength: 1, maxLength: 255 },
        country_id: { type: 'string', format: 'uuid' },
      },
    };
  }

  static get relationMappings() {
    const Client = require('./User');
    const Country = require('./Country');

    return {
      collaborators: {
        relation: Model.HasManyRelation,
        modelClass: Client,
        join: {
          from: 'company.company_nif',
          to: 'user.company_nif',
        },
      },
      country: {
        relation: Model.BelongsToOneRelation,
        modelClass: Country,
        join: {
          from: 'company.country_id',
          to: 'country.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = Company;
