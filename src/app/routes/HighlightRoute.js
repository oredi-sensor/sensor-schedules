const HighlightController = require('../controllers/HighlightController');
const GetHighlightValidation = require('../validations/GetHighlightValidation');

module.exports = [
  {
    method: 'GET',
    path: '/highlights',
    options: {
      auth: 'auth',
      validate: GetHighlightValidation,
    },
    handler: (req) => {
      const { query, headers } = req;
      const { public_key } = headers;
      const { user_id } = req.auth.credentials;

      return HighlightController.getHighlight(
        { ...query, user_id },
        { public_key }
      );
    },
  },
];
