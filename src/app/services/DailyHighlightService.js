const { Model, raw } = require('objection');
const knex = Model.knex();

const OutputMetaData = require('../models/MetaData/OutputMetaData');
const MobileAppOutput = require('../models/MobileApp/MobileAppOutput');
const MobileAppQuestionnaire = require('../models/MobileApp/MobileAppQuestionnaire');
const UserDailyContent = require('../models/MobileApp/UserDailyContent');
const MobileAppUserOutput = require('../models/MobileApp/MobileAppUserOutput');
const DailyContentConfig = require('../models/MobileApp/DailyContentConfig');
const Output = require('../models/Output/Output');
const Question = require('../models/Questionnaire/Question');
const UserQuestionnaireAnswers = require('../models/Questionnaire/UserQuestionnaireAnswers');
const EntityStatus = require('../models/EntityStatus');
const UserInputSelected = require('../models/Alg/UserInputSelected');

const getEntityState = async (state) => {
  const { id } = await EntityStatus.query().findOne('status', state);

  return id;
};

async function getWeekOfTheYear(extraDate) {
  let now = new Date();
  if (extraDate) now = new Date(extraDate);
  const onejan = new Date(now.getFullYear(), 0, 1);
  return Math.ceil(
    ((now.getTime() - onejan.getTime()) / 86400000 + onejan.getDay() + 1) / 7
  );
}

function unique(data) {
  let unique = [];
  let index = 0;
  for (const item of data) {
    const oldU = unique;
    unique = [];

    for (const value of item) {
      if (index === 0) {
        unique.push([value]);
        continue;
      }

      for (const combination of oldU) {
        if (combination.indexOf(value) === -1) {
          unique.push([...combination, value]);
        } else {
          unique.push([...combination, null]);
        }
      }
    }
    index++;
  }

  return unique;
}

async function getUserTodayInputs(user_id, questionnaire_id) {
  // todo: validade user & questionnaire
  return new Promise(async (resolve, reject) => {
    try {
      const todayInput = await UserInputSelected.query()
        .where('created_at', '>=', raw('now()::date'))
        .findOne({
          user_id,
        });

      if (todayInput) {
        throw new Error('Invalid selected input');
      }

      const data = (
        await knex.raw('select * from getPossibleInputs(?);', [
          questionnaire_id,
        ])
      ).rows;

      if (!data || data.length < 1) {
        throw new Error('Empty possible inputs');
      }

      const criteriaDb = data.reduce(function (r, a) {
        r[a.criteria_id] = r[a.criteria_id] || [];
        r[a.criteria_id].push(a.input_id);
        return r;
      }, Object.create(null));

      const inputsDb = data.reduce(function (r, a) {
        r[a.input_id] = r[a.input_id] || [];
        r[a.input_id].push(a);
        return r;
      }, Object.create(null));

      const inputsOptions = unique(
        Object.keys(criteriaDb).map((i) => criteriaDb[i])
      ).sort((a, b) => {
        return b.filter(Boolean).length - a.filter(Boolean).length;
      });
      const inputs = inputsOptions[0];

      const criteriaKeys = Object.keys(criteriaDb);

      const inputsFormatted = inputs
        .filter((item) => item)
        .map((input_id, index) => {
          return {
            criteria_id: criteriaKeys[index],
            input_id,
            image_id: inputsDb[input_id][0].image_id,
          };
        });

      resolve(inputsFormatted);
    } catch (e) {
      reject(e);
    }
  });
}

async function checkUserDailyContent(user_id, extraDate) {
  let d = new Date();
  if (extraDate) d = new Date(extraDate);
  const year = d.getFullYear();
  const day = d.getDay();
  const weekOfYear = await getWeekOfTheYear(extraDate);

  const dailyContent = await UserDailyContent.query()
    .where({
      year: year,
      week_of_year: weekOfYear,
      day_of_week: day,
      user_id: user_id,
      // highlight_type: highlight,
    })
    .orderBy('created_at', 'desc')
    .limit(1);

  if (dailyContent[0] && dailyContent[0].completed === true) {
    return 'completed';
  }

  if (!dailyContent[0]) return 'not_found';

  return {
    id: dailyContent[0].id,
    highlight_type: dailyContent[0].highlight_type,
  };
}

async function insertUserDailyContent(user_id, highlight, extraDate) {
  let d = new Date();
  if (extraDate) d = new Date(extraDate);

  const year = d.getFullYear();
  const day = d.getDay();
  const weekOfYear = await getWeekOfTheYear(extraDate);
  const userDailyContent = await UserDailyContent.query().insert({
    year: year,
    week_of_year: weekOfYear,
    day_of_week: day,
    user_id: user_id,
    highlight_type: highlight,
    completed: false,
    created_at: extraDate, // to test
  });

  if (extraDate)
    await UserDailyContent.query().patchAndFetchById(userDailyContent.id, {
      created_at: extraDate,
    });

  return userDailyContent;
}

async function getPrinciple(questionnaire_answer_id) {
  const principles_avg = await knex.raw(
    `
      select ucrbp.avg_reactivity, p.code, p.name as name_id, p.id as id from user_criteria_results_by_principle  ucrbp
      left join alg_principle p on p.name = ucrbp.principle  where ucrbp.id = ?
      group by ucrbp.avg_reactivity, p.code, p.id order by ucrbp.avg_reactivity

    `,
    [questionnaire_answer_id]
  );

  const avgPrinciples =
    principles_avg.rows.reduce(
      (acc, { avg_reactivity }) => acc + +avg_reactivity,
      0
    ) / principles_avg.rows.length;

  const [lessAvgPrinciple] = principles_avg.rows;
  const [greaterAvgPrinciple] = principles_avg.rows.reverse();

  let principleSelected = null;

  if (
    Math.abs(+lessAvgPrinciple.avg_reactivity - avgPrinciples) >
    Math.abs(+greaterAvgPrinciple.avg_reactivity - avgPrinciples)
  ) {
    principleSelected = lessAvgPrinciple;
  } else {
    principleSelected = greaterAvgPrinciple;
  }
  return principleSelected.id;
}

async function repeatFocusInsight(use_case, principle) {
  let output = await OutputMetaData.query()
    .leftJoinRelation('output')
    .where('output.output_use_case', use_case)
    .andWhere({ principle_id: principle })
    .select('output_id');

  if (!output.length) throw new Error('OUTPUT_WITH_PRINCIPLE_NOT_FOUND');

  const indexRandom = (Math.random() * (output.length - 1)).toFixed(0);

  output = output[indexRandom];

  return output;
}

async function focusInsight(
  current,
  extraDate,
  config,
  type,
  user_id,
  language,
  questionnaire_answer_id,
  mobileAppId,
  expireDays
) {
  const activeStatusId = await getEntityState('ACTIVE');
  const use_case = 'ALGORITHM';

  let todayOutput = null;

  if (current !== 'completed' && current !== 'not_found' && !type) {
    todayOutput = await MobileAppUserOutput.query()
      .where(
        'mobile_app_user_output.created_at',
        '>=',
        raw(`(now()- interval '${expireDays} days')::date`, [])
      )
      .findOne({
        user_id,
        highlight_type: 'WELCOME',
      })
      .orderBy('created_at', 'desc')
      .limit(1)
      .withGraphFetched(
        '[output.[entityComponent.[component], mobileAppOutput(onlyActive), images(onlyId), texts.[translations(filterByCurrentLanguage)],nameOutput.[translations(filterByCurrentLanguage)]]]'
      )
      .modifiers({
        filterByCurrentLanguage: (query) =>
          query.modify('filterLanguage', language),
        onlyActive(builder) {
          builder.where('status_id', activeStatusId);
        },
        onlyId(builder) {
          builder.select('id');
        },
      });
  }

  if (todayOutput) return todayOutput.output;

  const principle = await getPrinciple(questionnaire_answer_id);

  if (!principle) throw new Error('PRINCIPLE_NOT_FOUND');

  let output = await OutputMetaData.query()
    .leftJoinRelation('output')
    .where('output.output_use_case', use_case)
    .andWhere({ principle_id: principle })
    .andWhere(
      raw(
        'not exists(select 1\n' +
          'from mobile_app_user_output mauo ' +
          'where mauo.output_id = output_meta_data.output_id ' +
          // '  and mauo.highlight_type = ? ' +
          '  and mauo.mobile_app_id = ? ' +
          '  and mauo.user_id = ?)',
        /* highlight_type, */ mobileAppId,
        user_id
      )
    )
    .select('output_id');

  if (output.length === 0) {
    if (config.repeat) {
      output = await repeatFocusInsight(use_case, principle);
    } else {
      throw new Error('NOT_FOUND');
    }
  } else {
    const indexRandom = (Math.random() * (output.length - 1)).toFixed(0);
    output = output[indexRandom];
  }

  const outputSelected = await Output.query()
    .findById(output.output_id)
    .withGraphFetched(
      '[entityComponent.[component], images(onlyId), texts.[translations(filterByCurrentLanguage)],nameOutput.[translations(filterByCurrentLanguage)]]'
    )
    .modifiers({
      filterByCurrentLanguage: (query) =>
        query.modify('filterLanguage', language),
      onlyActive(builder) {
        builder.where('status_id', activeStatusId);
      },
      onlyId(builder) {
        builder.select('id');
      },
    });

  if (!outputSelected) {
    throw new Error('NO_OUTPUT_FOR_HIGHLIGHT');
  }

  const newOutput = await MobileAppUserOutput.query().insert({
    output_id: outputSelected.id,
    user_id,
    mobile_app_id: mobileAppId,
    highlight_type: 'WELCOME',
  });

  if (extraDate)
    await MobileAppUserOutput.query().patchAndFetchById(newOutput.id, {
      created_at: extraDate,
    });

  return outputSelected;
}

async function repeatGeneralInsights(language, activeStatusId) {
  let output = await MobileAppOutput.query()
    .where('mobile_app_output.action', null)
    .innerJoinRelated('highlightTypes')
    .leftJoinRelated('entityStatus')
    .where('highlightTypes.highlight_type', 'WELCOME')
    .where('entityStatus.status', 'ACTIVE')
    .andWhere(function (builder) {
      builder.where((builder) => {
        builder
          .where(
            raw(
              'not exists(select 1\n' +
                'from output_meta_data omd ' +
                'where omd.output_id = mobile_app_output.output_id) '
            )
          )
          .orWhere(
            knex.raw(
              'exists(select 1\n' +
                'from output_meta_data omd ' +
                'where omd.output_id = mobile_app_output.output_id ' +
                'and omd.action is NULL)'
            )
          );
      });
    })
    .withGraphFetched(
      '[output.[entityComponent.[component], mobileAppOutput(onlyActive), images(onlyId), texts.[translations(filterByCurrentLanguage)],nameOutput.[translations(filterByCurrentLanguage)]]]'
    )
    .modifiers({
      filterByCurrentLanguage: (query) =>
        query.modify('filterLanguage', language),
      onlyActive(builder) {
        builder.where('status_id', activeStatusId);
      },
      onlyId(builder) {
        builder.select('id');
      },
    });

  const indexRandom = (Math.random() * (output.length - 1)).toFixed(0);

  output = output[indexRandom];

  return output;
}

async function generalInsights(
  current,
  extraDate,
  config,
  type,
  language,
  user_id,
  expireDays,
  mobileAppId
) {
  const activeStatusId = await getEntityState('ACTIVE');

  let todayOutput = null;

  if (current !== 'completed' && current !== 'not_found' && !type) {
    todayOutput = await MobileAppUserOutput.query()
      .where(
        'mobile_app_user_output.created_at',
        '>=',
        raw(`(now()- interval '${expireDays} days')::date`, [])
      )
      .findOne({
        user_id,
        highlight_type: 'WELCOME',
      })
      .orderBy('created_at', 'desc')
      .limit(1)
      .withGraphFetched(
        '[output.[entityComponent.[component], mobileAppOutput(onlyActive), images(onlyId), texts.[translations(filterByCurrentLanguage)],nameOutput.[translations(filterByCurrentLanguage)]]]'
      )
      .modifiers({
        filterByCurrentLanguage: (query) =>
          query.modify('filterLanguage', language),
        onlyActive(builder) {
          builder.where('status_id', activeStatusId);
        },
        onlyId(builder) {
          builder.select('id');
        },
      });
  }

  if (todayOutput) return todayOutput.output;

  const mobileAppOutput = await MobileAppOutput.query()
    .whereNull('mobile_app_output.action')
    .innerJoinRelated('highlightTypes')
    .leftJoinRelated('entityStatus')
    .where('highlightTypes.highlight_type', 'WELCOME')
    .where('entityStatus.status', 'ACTIVE')
    .leftJoinRelated('output')
    .where('output.output_use_case', 'GENERAL')
    .andWhere(
      raw(
        'not exists(select 1\n' +
          'from mobile_app_user_output mauo ' +
          'where mauo.output_id = mobile_app_output.output_id ' +
          // '  and mauo.highlight_type = ? ' +
          '  and mauo.mobile_app_id = ? ' +
          '  and mauo.user_id = ?)',
        /* highlight_type, */ mobileAppId,
        user_id
      )
    )
    .andWhere(function (builder) {
      builder.where((builder) => {
        builder
          .where(
            raw(
              'not exists(select 1\n' +
                'from output_meta_data omd ' +
                'where omd.output_id = mobile_app_output.output_id) '
            )
          )
          .orWhere(
            knex.raw(
              'exists(select 1\n' +
                'from output_meta_data omd ' +
                'where omd.output_id = mobile_app_output.output_id ' +
                'and omd.action is NULL)'
            )
          );
      });
    })
    .withGraphFetched(
      '[output(onlyGeneral).[entityComponent.[component], mobileAppOutput(onlyActive), images(onlyId), texts.[translations(filterByCurrentLanguage)],nameOutput.[translations(filterByCurrentLanguage)]]]'
    )
    .modifiers({
      filterByCurrentLanguage: (query) =>
        query.modify('filterLanguage', language),
      onlyActive(builder) {
        builder.where('status_id', activeStatusId);
      },
      onlyGeneral(builder) {
        builder.where('output_use_case', 'GENERAL');
      },
      onlyId(builder) {
        builder.select('id');
      },
    });

  if (!mobileAppOutput.length) {
    if (config.repeat) {
      todayOutput = await repeatGeneralInsights(language, activeStatusId);
    } else {
      throw new Error('Not found');
    }
  } else {
    const indexRandom = (Math.random() * (mobileAppOutput.length - 1)).toFixed(
      0
    );
    todayOutput = mobileAppOutput[indexRandom];
  }

  if (!todayOutput) {
    throw new Error('Not found');
  }

  const newOutput = await MobileAppUserOutput.query().insert({
    output_id: todayOutput.output_id,
    user_id,
    mobile_app_id: mobileAppId,
    highlight_type: 'WELCOME',
  });

  if (extraDate) {
    await MobileAppUserOutput.query().patchAndFetchById(newOutput.id, {
      created_at: extraDate,
    });
  }

  return todayOutput.output;
}

async function repeatGeneralCriteria(language, activeStatusId, action) {
  const mobileAppOutput = await MobileAppOutput.query()
    .innerJoinRelated('highlightTypes')
    .leftJoinRelated('entityStatus')
    .where('highlightTypes.highlight_type', 'WELCOME')
    .andWhere('entityStatus.status', 'ACTIVE')
    /*
    .andWhere(
      knex.raw(
        'exists(select 1\n' +
          'from output_meta_data omd ' +
          'where omd.output_id = mobile_app_output.output_id ' +
          'and omd.criteria_id is NOT NULL ' +
          'and omd.action = ?)',
        [true],
      ),
    ) */
    .andWhere('mobile_app_output.action', action)
    .orderBy('index', 'asc')
    .first()
    .withGraphFetched(
      '[output.[criteria(onlyAction).[stringResources.[translations(filterByCurrentLanguage)]], entityComponent.[component], mobileAppOutput(onlyActive), images(onlyId), texts.[translations(filterByCurrentLanguage)],nameOutput.[translations(filterByCurrentLanguage)]]]'
    )
    .modifiers({
      filterByCurrentLanguage: (query) =>
        query.modify('filterLanguage', language),
      onlyActive(builder) {
        builder.where('status_id', activeStatusId);
      },
      onlyId(builder) {
        builder.select('id');
      },
      onlyAction(builder) {
        builder.where('action', true);
      },
    });

  // const indexRandom = (Math.random() * (mobileAppOutput.length - 1)).toFixed(0);

  // mobileAppOutput = mobileAppOutput[indexRandom];

  return mobileAppOutput;
}

async function generalCriteria(
  current,
  type,
  extraDate,
  config,
  extraType,
  language,
  user_id,
  expireDays,
  mobileAppId
) {
  const actionsTypes = {
    criteria: 'GO_TO_CRITERIAS',
    criteriaSelf: 'GO_TO_CRITERIAS_SELF',
    criteriaProfessional: 'GO_TO_CRITERIAS_PROFESSIONAL',
    criteriaSocial: 'GO_TO_CRITERIAS_SOCIAL',
  };

  const action = actionsTypes[type];

  const activeStatusId = await getEntityState('ACTIVE');

  let todayOutput = null;

  if (current !== 'completed' && current !== 'not_found' && !extraType) {
    todayOutput = await MobileAppUserOutput.query()
      .where(
        'mobile_app_user_output.created_at',
        '>=',
        raw(`(now()- interval '${expireDays} days')::date`, [])
      )
      .findOne({
        user_id,
        highlight_type: 'WELCOME',
      })
      .orderBy('created_at', 'desc')
      .limit(1)
      .withGraphFetched(
        '[output.[criteria(onlyAction).[stringResources.[translations(filterByCurrentLanguage)]],' +
          'entityComponent.[component], mobileAppOutput(onlyActive), images(onlyId), texts.[translations(filterByCurrentLanguage)],nameOutput.[translations(filterByCurrentLanguage)]]]'
      )
      .modifiers({
        filterByCurrentLanguage: (query) =>
          query.modify('filterLanguage', language),
        onlyActive(builder) {
          builder.where('status_id', activeStatusId);
        },
        onlyId(builder) {
          builder.select('id');
        },
        onlyAction(builder) {
          builder.where('action', true);
        },
      });
  }

  if (todayOutput) return todayOutput.output;

  const mobileAppOutput = await MobileAppOutput.query()
    .innerJoinRelated('highlightTypes')
    .leftJoinRelated('entityStatus')
    .where('highlightTypes.highlight_type', 'WELCOME')
    .andWhere('entityStatus.status', 'ACTIVE')
    .andWhere(
      raw(
        'not exists(select 1\n' +
          'from mobile_app_user_output mauo ' +
          'where mauo.output_id = mobile_app_output.output_id ' +
          // '  and mauo.highlight_type = ? ' +
          '  and mauo.mobile_app_id = ? ' +
          '  and mauo.user_id = ?)',
        /* highlight_type, */ mobileAppId,
        user_id
      )
    ) /*
    .andWhere(
      knex.raw(
        'exists(select 1\n' +
          'from output_meta_data omd ' +
          'where omd.output_id = mobile_app_output.output_id ' +
          'and omd.criteria_id is NOT NULL ' +
          'and omd.action = ?)',
        [true],
      ),
    ) */
    .andWhere('mobile_app_output.action', action)
    .orderBy('index', 'asc')
    .first()
    .withGraphFetched(
      '[output.[criteria(onlyAction).[stringResources.[translations(filterByCurrentLanguage)]], entityComponent.[component], mobileAppOutput(onlyActive), images(onlyId), texts.[translations(filterByCurrentLanguage)],nameOutput.[translations(filterByCurrentLanguage)]]]'
    )
    .modifiers({
      filterByCurrentLanguage: (query) =>
        query.modify('filterLanguage', language),
      onlyActive(builder) {
        builder.where('status_id', activeStatusId);
      },
      onlyId(builder) {
        builder.select('id');
      },
      onlyAction(builder) {
        builder.where('action', true);
      },
    });

  if (!mobileAppOutput) {
    if (config.repeat) {
      todayOutput = await repeatGeneralCriteria(
        language,
        activeStatusId,
        action
      );
    } else {
      throw new Error('Not found');
    }
  } else {
    todayOutput = mobileAppOutput;

    if (!todayOutput) {
      throw new Error('Not found');
    }
  }

  const newOutput = await MobileAppUserOutput.query().insert({
    output_id: todayOutput.output_id,
    user_id,
    mobile_app_id: mobileAppId,
    highlight_type: 'WELCOME',
    created_at: extraDate, // to test
  });

  if (extraDate)
    await MobileAppUserOutput.query().patchAndFetchById(newOutput.id, {
      created_at: extraDate,
    });

  return todayOutput.output;
}

async function repeatActQuiz(language, mobileAppId, activeStatusId) {
  const quiz = await MobileAppQuestionnaire.query()

    .leftJoinRelated('questionnaire.[entityStatus]')
    .innerJoinRelated('highlightTypes')
    .where({
      'mobile_app_questionnaire.mobile_app_id': mobileAppId,
    })
    .where('questionnaire:entityStatus.status', 'ACTIVE')
    .where('mobile_app_questionnaire.status_id', activeStatusId)
    .where('highlightTypes.highlight_type', 'WELCOME')

    .withGraphFetched(
      '[questionnaire(onlyActive).[descriptions.[translations(filterByCurrentLanguage).[language]], stringResources.[translations(filterByCurrentLanguage).[language]],entityStatus]]'
    )
    .groupBy('mobile_app_questionnaire.id')
    .modifiers({
      filterByCurrentLanguage: (query) =>
        query.modify('filterLanguage', language),
      onlyActive(builder) {
        builder.where('status_id', activeStatusId);
      },
    });

  return quiz;
}

async function actQuiz(language, user_id, config, mobileAppId) {
  const activeStatusId = await getEntityState('ACTIVE');

  const { rows: questionnairesInProgress } = await knex.raw(
    `select *
        from questionnaire_answer qa
        left join mobile_app_env mae on mae.entity_id = qa.questionnaire_id
        where qa.user_id = ?
        and qa.status_id = ?
        and mae.env_name <> 'PERSONAL_INFO_QUESTIONNAIRE'
        and mae.env_name <> 'MAIN_QUESTIONNAIRE'
        and (
          (completed = true and to_char(completed_time, 'YYYY-MM-DD')::date > to_char(now()- interval '7 days', 'YYYY-MM-DD')::date)
          or
          completed = false 
          ) 
          `,

    [user_id, activeStatusId]
  );

  const [{ questionnaire_id, completed } = {}] = questionnairesInProgress || [];

  if (completed) {
    throw new Error('ACT_ANSWERED_LAST_7_DAYS');
  }

  const query =
    questionnaire_id && !completed
      ? { 'questionnaire_answer.questionnaire_id': questionnaire_id }
      : { 'questionnaire_answer.id': null };

  const quiz = await MobileAppQuestionnaire.query()
    .where(query)
    .leftJoinRelated('questionnaire.[entityStatus]')
    .leftJoin('questionnaire_answer', function () {
      this.on('questionnaire.id', 'questionnaire_answer.questionnaire_id')
        .andOn('questionnaire_answer.user_id', '=', knex.raw('?', user_id))
        // .andOn('questionnaire_answer.completed', '=', knex.raw('?', true))
        .andOn(
          'questionnaire_answer.status_id',
          '=',
          knex.raw('?', activeStatusId)
        );
    })
    .innerJoinRelated('highlightTypes')
    .where({
      'mobile_app_questionnaire.mobile_app_id': mobileAppId,
    })
    .where('questionnaire:entityStatus.status', 'ACTIVE')
    .where('mobile_app_questionnaire.status_id', activeStatusId)
    .where('highlightTypes.highlight_type', 'WELCOME')

    .withGraphFetched(
      '[questionnaire(onlyActive).[descriptions.[translations(filterByCurrentLanguage).[language]], stringResources.[translations(filterByCurrentLanguage).[language]],entityStatus]]'
    )
    .groupBy('mobile_app_questionnaire.id')
    .modifiers({
      filterByCurrentLanguage: (query) =>
        query.modify('filterLanguage', language),
      onlyActive(builder) {
        builder.where('status_id', activeStatusId);
      },
    });

  if (quiz && quiz.length) return quiz;
  else if (config.repeat) {
    return await repeatActQuiz(language, mobileAppId, activeStatusId);
  } else {
    throw new Error('NOT_FOUND');
  }
}

async function repeatReportReact(language, activeStatusId) {
  const report = await MobileAppOutput.query()
    .where('action', 'REPORT_REACT')
    .orderBy('index', 'asc')
    .first()
    .withGraphFetched(
      '[output.[entityComponent.[component], mobileAppOutput(onlyActive), images(onlyId), texts.[translations(filterByCurrentLanguage)],nameOutput.[translations(filterByCurrentLanguage)]]]'
    )
    .modifiers({
      filterByCurrentLanguage: (query) =>
        query.modify('filterLanguage', language),
      onlyActive(builder) {
        builder.where('status_id', activeStatusId);
      },
    });

  // const indexRandom = (Math.random() * (report.length - 1)).toFixed(0);

  // report = report[indexRandom];

  return report;
}

async function reportReact(
  current,
  extraDate,
  config,
  type,
  user_id,
  language,
  expireDays,
  mobileAppId
) {
  const activeStatusId = await getEntityState('ACTIVE');

  let todayOutput = null;

  if (current !== 'completed' && current !== 'not_found' && !type) {
    todayOutput = await MobileAppUserOutput.query()
      .where(
        'mobile_app_user_output.created_at',
        '>=',
        raw(`(now()- interval '${expireDays} days')::date`, [])
      )
      .findOne({
        user_id,
        highlight_type: 'WELCOME',
      })
      .orderBy('created_at', 'desc')
      .limit(1)
      .withGraphFetched(
        '[output.[entityComponent.[component], mobileAppOutput(onlyActive), images(onlyId), texts.[translations(filterByCurrentLanguage)],nameOutput.[translations(filterByCurrentLanguage)]]]'
      )
      .modifiers({
        filterByCurrentLanguage: (query) =>
          query.modify('filterLanguage', language),
        onlyActive(builder) {
          builder.where('status_id', activeStatusId);
        },
        onlyId(builder) {
          builder.select('id');
        },
        onlyAction(builder) {
          builder.where('action', true);
        },
      });
  }

  if (todayOutput) return todayOutput.output;

  const report = await MobileAppOutput.query()
    .where(
      raw(
        'not exists(select 1\n' +
          'from mobile_app_user_output mauo ' +
          'where mauo.output_id = mobile_app_output.output_id ' +
          // '  and mauo.highlight_type = ? ' +
          '  and mauo.mobile_app_id = ? ' +
          '  and mauo.user_id = ?)',
        /* highlight_type, */ mobileAppId,
        user_id
      )
    )
    .andWhere('action', 'REPORT_REACT')
    .orderBy('index', 'asc')
    .first()
    .withGraphFetched(
      '[output.[entityComponent.[component], mobileAppOutput(onlyActive), images(onlyId), texts.[translations(filterByCurrentLanguage)],nameOutput.[translations(filterByCurrentLanguage)]]]'
    )
    .modifiers({
      filterByCurrentLanguage: (query) =>
        query.modify('filterLanguage', language),
      onlyActive(builder) {
        builder.where('status_id', activeStatusId);
      },
    });

  if (!report) {
    if (config.repeat) {
      todayOutput = await repeatReportReact(language, activeStatusId);
    } else {
      throw new Error('REPORT_NOT_FOUND');
    }
  } else {
    // const indexRandom = (Math.random() * (report.length - 1)).toFixed(0);
    // todayOutput = report[indexRandom];

    todayOutput = report;
  }

  if (!todayOutput) {
    throw new Error('REPORT_NOT_FOUND');
  }

  const newOutput = await MobileAppUserOutput.query().insert({
    output_id: todayOutput.output_id,
    user_id,
    mobile_app_id: mobileAppId,
    highlight_type: 'WELCOME',
    created_at: extraDate, // to test
  });

  if (extraDate)
    await MobileAppUserOutput.query().patchAndFetchById(newOutput.id, {
      created_at: extraDate,
    });

  return todayOutput;
}

async function DailyHighlightService({
  type,
  language,
  mobileAppId,
  questionnaire_answer_id,
  user_id,
}) {
  return new Promise(async (resolve, reject) => {
    try {
      const d = new Date();
      const expireDays = 1;
      const extraDate = new Date();

      const highlightsScheduled = await DailyContentConfig.query().orderBy(
        'week_index',
        'desc'
      );

      const dayOfTheWeek = d.getDay();
      // if (type) dayOfTheWeek = 9;
      const date = d.getDate();
      const weekOfMonth = Math.ceil((date + 6 - dayOfTheWeek) / 7);
      let weekOfMonthIndex = weekOfMonth;
      weekOfMonthIndex--;

      const maxWeekIndex = highlightsScheduled[0].week_index;

      let highlightsOfTheDay = [];

      if (maxWeekIndex === 0) {
        highlightsOfTheDay = highlightsScheduled.filter(
          (h) => h.day_of_week === dayOfTheWeek
        );
      } else if (maxWeekIndex > 5) {
        highlightsOfTheDay = highlightsScheduled.filter(
          (h) =>
            h.day_of_week === dayOfTheWeek &&
            h.week_index % 6 === weekOfMonthIndex
        );
      } else {
        highlightsOfTheDay = highlightsScheduled.filter(
          (h) =>
            h.day_of_week === dayOfTheWeek &&
            h.week_index === weekOfMonthIndex % (maxWeekIndex + 1)
        );
      }
      if (!highlightsOfTheDay[0]) {
        throw new Error('No highlights for today detected');
      }

      // For testing change the value under this line for the highlight output that you want to test
      /**
       * Highlights:
       *  General Insights
       *  Resources of focus insights
       *  Act Quiz call to action
       *  General Criteria
       *  Self Criteria
       *  Social Criteria
       *  Professional Criteria
       *  Report and React
       *  Heart (Re)Question
       */

      highlightsOfTheDay[0].highlight = type || highlightsOfTheDay[0].highlight;
      console.log(
        `############# ${highlightsOfTheDay[0].highlight} #############`
      );
      const config = {
        general: { repeat: true },
        algorithm: { repeat: true },
        criteria: { repeat: true },
        criteriaSelf: { repeat: true },
        criteriaSocial: { repeat: true },
        criteriaProfessional: { repeat: true },
        report: { repeat: true },
        quiz: { repeat: true },
      };

      if (highlightsOfTheDay[0].highlight === 'Heart (Re)Question') {
        let userDailyContent = await checkUserDailyContent(user_id, extraDate);
        // check if the user answered all the questions of the heart questionnaire

        let questionsClosedAnswered = await UserQuestionnaireAnswers.query()
          .where({
            questionnaire_answer_id: questionnaire_answer_id,
            user_id: user_id,
            checked: true,
          })
          .select('question_id')
          .groupBy('question_id')
          .havingRaw('count(question_id) > ?', 0);

        questionsClosedAnswered = questionsClosedAnswered.map(
          (q) => q.question_id
        );

        let questions = await Question.query()
          .select(
            'question.id',
            'user_questionnaire_answers.questionnaire_answer_id',
            'user_questionnaire_answers.questionnaire_id',
            'user_questionnaire_answers.questionnaire_category_id',
            'question.is_open',
            'question.is_multiple'
          )
          .leftJoinRelated('entityStatus')
          .where('entityStatus.status', 'ACTIVE')
          .leftJoin(
            'user_questionnaire_answers',
            'question.id',
            'user_questionnaire_answers.question_id'
          )
          .andWhere(
            'user_questionnaire_answers.questionnaire_answer_id',
            questionnaire_answer_id
          )
          .andWhere('user_questionnaire_answers.user_id', user_id)
          .whereNotIn(
            'user_questionnaire_answers.question_id',
            questionsClosedAnswered
          )
          .orWhere((builder) =>
            builder
              .whereNull('user_questionnaire_answers.closed_answer_id')
              .andWhere(
                'user_questionnaire_answers.questionnaire_answer_id',
                questionnaire_answer_id
              )
              .andWhere('user_questionnaire_answers.user_id', user_id)
              .whereNull('user_questionnaire_answers.text_answer')
          )
          .withGraphFetched(
            '[videos, answerType,text.[translations.[language]],subTexts.[translations.[language]],images(onlyId),tags.[tag.[stringResources.[translations.[language]]]],entityStatus, answerOptions(notDeleted, orderByIndex).[video, stringResources.[translations(filterByCurrentLanguage).[language]], image(onlyId), entityStatus]]'
          )
          .modifiers({
            filterByCurrentLanguage: (query) =>
              query.modify('filterLanguage', language),
          })
          .groupBy(
            'question.id',
            'user_questionnaire_answers.questionnaire_answer_id',
            'user_questionnaire_answers.questionnaire_id',
            'user_questionnaire_answers.questionnaire_category_id',
            'question.is_open',
            'question.is_multiple'
          );

        if (!questions.length) {
          const outputReport = await reportReact(
            userDailyContent,
            extraDate,
            config.report,
            type,
            user_id,
            language,
            expireDays,
            mobileAppId
          );

          if (!userDailyContent.id)
            userDailyContent = await insertUserDailyContent(
              user_id,
              'report',
              extraDate
            );

          resolve({
            type: 'report',
            id: userDailyContent.id,
            data: outputReport,
          });
        } else {
          const indexRandom = (Math.random() * (questions.length - 1)).toFixed(
            0
          );

          questions = questions[indexRandom];

          if (!userDailyContent.id)
            userDailyContent = await insertUserDailyContent(
              user_id,
              'question',
              extraDate
            );

          resolve({
            type: 'question',
            id: userDailyContent.id,
            data: { question: questions },
          });
        }
      }

      if (highlightsOfTheDay[0].highlight === 'Daily Challenge') {
        // daily challenge

        const dailyChallenge = await getUserTodayInputs(
          user_id,
          questionnaire_answer_id
        );

        const userDailyContent = await checkUserDailyContent(
          user_id,
          extraDate
        );

        resolve({
          type: 'dailyChallenge',
          id: userDailyContent.id,
          data: { dailyChallenge },
        });
      }

      if (highlightsOfTheDay[0].highlight === 'General Insights') {
        // General Insights

        let userDailyContent = await checkUserDailyContent(user_id, extraDate);

        const todayOutput = await generalInsights(
          userDailyContent,
          extraDate,
          config.general,
          type,
          language,
          user_id,
          expireDays,
          mobileAppId
        );

        if (!userDailyContent.id)
          userDailyContent = await insertUserDailyContent(
            user_id,
            'general',
            extraDate
          );

        resolve({
          type: 'general',
          data: todayOutput,
          id: userDailyContent.id,
        });
      }

      if (highlightsOfTheDay[0].highlight === 'Resources of focus insights') {
        // Resource on Focus Insight
        let userDailyContent = await checkUserDailyContent(user_id, extraDate);

        const todayOutput = await focusInsight(
          userDailyContent,
          extraDate,
          config.algorithm,
          type,
          user_id,
          language,
          questionnaire_answer_id,
          mobileAppId,
          expireDays
        );

        if (!userDailyContent.id)
          userDailyContent = await insertUserDailyContent(
            user_id,
            'algorithm',
            extraDate
          );

        resolve({
          type: 'algorithm',
          data: todayOutput,
          id: userDailyContent.id,
        });
      }

      if (highlightsOfTheDay[0].highlight === 'Act Quiz call to action') {
        let userDailyContent = await checkUserDailyContent(user_id, extraDate);

        if (!userDailyContent.id)
          userDailyContent = await insertUserDailyContent(
            user_id,
            'quiz',
            extraDate
          );

        // Quiz

        let quiz = await actQuiz(language, user_id, config.quiz, mobileAppId);

        if (!quiz) throw new Error('ACT_ANSWERED_LAST_7_DAYS');

        const indexRandom = (Math.random() * (quiz.length - 1)).toFixed(0);
        quiz = quiz[indexRandom];

        resolve({
          type: 'quiz',
          data: quiz,
          id: userDailyContent.id,
        });
      }

      if (highlightsOfTheDay[0].highlight === 'General Criteria') {
        // Criteria Call to Action

        let userDailyContent = await checkUserDailyContent(user_id, extraDate);

        const todayOutput = await generalCriteria(
          userDailyContent,
          'criteria',
          extraDate,
          config.criteria,
          type,
          language,
          user_id,
          expireDays,
          mobileAppId
        );

        if (!userDailyContent.id)
          userDailyContent = await insertUserDailyContent(
            user_id,
            'criteria',
            extraDate
          );

        resolve({
          type: 'criteria',
          data: todayOutput,
          id: userDailyContent.id,
        });
      }

      if (highlightsOfTheDay[0].highlight === 'Self Criteria') {
        // Criteria Call to Action

        let userDailyContent = await checkUserDailyContent(user_id, extraDate);

        const todayOutput = await generalCriteria(
          userDailyContent,
          'criteriaSelf',
          extraDate,
          config.criteriaSelf,
          type,
          language,
          user_id,
          expireDays,
          mobileAppId
        );

        if (!userDailyContent.id)
          userDailyContent = await insertUserDailyContent(
            user_id,
            'criteriaSelf',
            extraDate
          );

        resolve({
          type: 'criteriaSelf',
          data: todayOutput,
          id: userDailyContent.id,
        });
      }

      if (highlightsOfTheDay[0].highlight === 'Social Criteria') {
        // Criteria Call to Action

        let userDailyContent = await checkUserDailyContent(user_id, extraDate);

        const todayOutput = await generalCriteria(
          userDailyContent,
          'criteriaSocial',
          extraDate,
          config.criteriaSocial,
          type,
          language,
          user_id,
          expireDays,
          mobileAppId
        );

        if (!userDailyContent.id)
          userDailyContent = await insertUserDailyContent(
            user_id,
            'criteriaSocial',
            extraDate
          );

        resolve({
          type: 'criteriaSocial',
          data: todayOutput,
          id: userDailyContent.id,
        });
      }

      if (highlightsOfTheDay[0].highlight === 'Professional Criteria') {
        // Criteria Call to Action

        let userDailyContent = await checkUserDailyContent(user_id, extraDate);

        const todayOutput = await generalCriteria(
          userDailyContent,
          'criteriaProfessional',
          extraDate,
          config.criteriaProfessional,
          type,
          language,
          user_id,
          expireDays,
          mobileAppId
        );

        if (!userDailyContent.id)
          userDailyContent = await insertUserDailyContent(
            user_id,
            'criteriaProfessional',
            extraDate
          );

        resolve({
          type: 'criteriaProfessional',
          data: todayOutput,
          id: userDailyContent.id,
        });
      }

      if (highlightsOfTheDay[0].highlight === 'Report and React') {
        // Report and React

        let userDailyContent = await checkUserDailyContent(user_id, extraDate);
        const outputReport = await reportReact(
          userDailyContent,
          extraDate,
          config.report,
          type,
          user_id,
          language,
          expireDays,
          mobileAppId
        );

        if (!userDailyContent.id)
          userDailyContent = await insertUserDailyContent(
            user_id,
            'report',
            extraDate
          );

        resolve({
          type: 'report',
          id: userDailyContent.id,
          data: outputReport,
        });
      }

      throw new Error('Daily highlight is empty');
    } catch (e) {
      reject(e);
    }
  });
}

exports.run = DailyHighlightService;
