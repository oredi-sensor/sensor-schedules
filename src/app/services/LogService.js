const knex = require('knex');

const sqliteConfig = require('../../config/logs/sqlite');

class LogService {
  constructor() {
    this.connection = knex(sqliteConfig);

    this.createTableIfNotExists();
  }

  createTableIfNotExists = async () => {
    const hasTable = await this.connection.schema.hasTable('logs');
    if (!hasTable) {
      await this.connection.schema.createTable('logs', function (table) {
        table.increments();
        table.uuid('user_id');
        table.string('request_data');
        table.string('responde_data');
        table.string('status_code');
        table.string('type');
        table.timestamps(true, true);
      });
    }
  };

  save = async ({
    request_data,
    status_code,
    response_data,
    user_id,
    type,
  }) => {
    await this.connection('logs').insert({
      user_id,
      type,
      request_data: JSON.stringify(request_data),
      responde_data: JSON.stringify(response_data),
      status_code: status_code,
    });
  };

  getAllNotificationsToday = async () => {
    const getCreatedAtRange = () => {
      const [today] = new Date().toISOString()?.split('T');
      let tomorrow = new Date();

      tomorrow.setDate(tomorrow.getDate() + 1);
      [tomorrow] = tomorrow.toISOString()?.split('T');

      return { from: today, to: tomorrow };
    };

    return new Promise(async (resolve, reject) => {
      const { from, to } = getCreatedAtRange();

      try {
        const notifications = await this.connection('logs')
          .where({
            type: 'notification',
          })
          .whereBetween('created_at', [from, to]);

        const notificationsFormatted = notifications.map((notification) => {
          return {
            ...notification,
            request_data: JSON.parse(notification.request_data),
            responde_data: JSON.parse(notification.responde_data),
          };
        });

        resolve(notificationsFormatted);
      } catch (e) {
        reject(e);
      }
    });
  };
}

module.exports = new LogService();
