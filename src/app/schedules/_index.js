const schedule = require('node-schedule');

const HighlightSchedule = require('./HighlightSchedule');
const SlackSchedule = require('./SlackSchedule');

schedule.scheduleJob({ minute: 23 }, HighlightSchedule.run);
schedule.scheduleJob({ hour: 13 }, SlackSchedule.run);
