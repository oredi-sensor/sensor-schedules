const LogService = require('../services/LogService');
const SlackService = require('../services/SlackService');

class SlackSchedule {
  async run() {
    const logs = await LogService.getAllNotificationsToday();

    const notifications = logs.map((log) => log.responde_data.notifications);

    const { usersCountSuccess, usersCountFails } = notifications.reduce(
      (previous, current) => {
        // (current?.some) todo:verify later
        if (current?.some((notification) => notification.success)) {
          return {
            ...previous,
            usersCountSuccess: previous.usersCountSuccess + 1,
          };
        }

        return { ...previous, usersCountFails: previous.usersCountFails + 1 };
      },
      {
        usersCountSuccess: 0,
        usersCountFails: 0,
      }
    );

    const {
      notificationsCountSuccess,
      notificationsCountFails,
    } = notifications.reduce(
      (previous, current) => {
        const success = current.filter((notification) => notification.success);
        const fails = current.filter((notification) => !notification.success);

        return {
          notificationsCountSuccess:
            previous.notificationsCountSuccess + success.length,
          notificationsCountFails:
            previous.notificationsCountFails + fails.length,
        };
      },
      {
        notificationsCountSuccess: 0,
        notificationsCountFails: 0,
      }
    );

    const notification = {
      username: 'Sensor API',
      text: 'Daily Highlight - Push Notification Statistics',
      icon_emoji: ':bangbang:',
      attachments: [
        {
          color: '#eed140',
          fields: [
            {
              title: '✅ Notifications Sent',
              value: notificationsCountSuccess,
              short: false,
            },
            {
              title: '🚨 Failed Notifications',
              value: notificationsCountFails,
              short: false,
            },
            {
              title: '✅ Users who received at least one notification',
              value: usersCountSuccess,
              short: false,
            },
            {
              title: '🚨 Users who have not received at least one notification',
              value: usersCountFails,
              short: false,
            },
          ],
        },
      ],
    };

    SlackService.sendMessage(notification, process.env.SLACK_WEB_HOOK);
  }
}

module.exports = new SlackSchedule();
