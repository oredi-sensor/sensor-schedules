const { raw } = require('objection');

const HighlightNotificationJob = require('../jobs/HighlightNotificationJob');

const EntityStatus = require('../models/EntityStatus');
const User = require('../models/User');
const MobileAppEnv = require('../models/MobileApp/MobileAppEnv');
const UserDailyContent = require('../models/MobileApp/UserDailyContent');

const Queue = require('../queue');

class HighlightSchedule {
  async run() {
    console.log('**HighlightSchedule**');
    const { id: status_id } = await EntityStatus.query().findOne(
      'status',
      'ACTIVE'
    );
    const mobileAppId = process.env.MOBILE_APP_ID;
    const user_id = 'e14313bf-6d25-4fbe-8335-dbf0e8090448';

    const {
      entity_id: mainQuestionnaireId,
    } = await MobileAppEnv.query().findOne({
      app_id: mobileAppId,
      env_name: 'MAIN_QUESTIONNAIRE',
    });

    const userDailyContent = UserDailyContent.query().where(
      'created_at',
      '>=',
      raw(`now() - (?*'1 HOUR'::INTERVAL)`, [1])
    );

    // todo: buscar apenas users sem highlight nas últimas 24hrs
    const users = await User.query()
      .withGraphFetched(
        '[userDevices(onlyActives), questionnaireAnswers(onlyMainQuestionnaire)]'
      )
      .where('user.status_id', status_id)
      .andWhere('user.id', user_id) // todo: remove this
      .whereNotExists(userDailyContent)
      .modifiers({
        onlyActives: (builder) => {
          return builder.where('active', true);
        },
        onlyMainQuestionnaire: (builder) => {
          return builder
            .where('questionnaire_id', mainQuestionnaireId)
            .orderBy('created_at', 'desc')
            .limit(1);
        },
      });

    users.forEach((user) => {
      Queue.add(HighlightNotificationJob.key, { user, mobileAppId });
    });

    Queue.process();

    return true;
  }
}

module.exports = new HighlightSchedule();
