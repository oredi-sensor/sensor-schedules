const firebase = require('firebase-admin');

const DailyHighlightService = require('../services/DailyHighlightService');

const Notification = require('../models/PushNotification/Notification');
const LogService = require('../services/LogService');

class HighlightNotificationJob {
  getNavigation({ type, data }) {
    // OUTPUT: 'general',
    // ALGORITHM: 'algorithm',
    // CRITERIA: 'criteria',
    // CRITERIA_SELF: 'criteriaSelf',
    // CRITERIA_SOCIAL: 'criteriaSocial',
    // CRITERIA_PROFESSIONAL: 'criteriaProfessional',
    // REPORT_AND_REACT: 'report',
    // QUIZ: 'quiz',
    // QUESTION: 'question',
    // DAILY_CHALLENGE: 'dailyChallenge'

    const routes = {
      general: {
        routeName: 'Home',
        params: { isHighlight: true },
      },
      algorithm: {
        routeName: 'Home',
        params: { isHighlight: true },
      },
      criteria: {
        routeName: 'Criterias',
        params: { isHighlight: true },
      },
      criteriaSelf: {
        routeName: 'Criterias',
        params: { isHighlight: true, active: 0 },
      },
      criteriaSocial: {
        routeName: 'Criterias',
        params: { isHighlight: true, active: 1 },
      },
      criteriaProfessional: {
        routeName: 'Criterias',
        params: { isHighlight: true, active: 2 },
      },
      report: {
        routeName: 'ReportAndReact',
        params: { isHighlight: true },
      },
      quiz: {
        routeName: 'ActQuestion',
        params: {
          isHighlight: true,
          questionnaire: {
            id: data?.questionnaire?.id,
            image: `${process.env.APP_URL}image/${data?.questionnaire?.image_id}`,
            title:
              data?.questionnaire?.stringResources?.translations?.[0]
                ?.translation,
            description:
              data?.questionnaire?.descriptions?.translations?.[0]?.translation,
          },
        },
      },
      question: {
        routeName: 'Home',
        params: { isHighlight: true },
      },
      dailyChallenge: {
        routeName: 'DailyChallenge',
        params: { isHighlight: true },
      },
    };

    return routes[type];
  }

  getBody({ type, data }) {
    if (type === 'quiz') {
      const [{ translation }] = data?.questionnaire?.descriptions
        ?.translations || [{}];

      return translation;
    }

    const { texts } = data || {};
    const [{ translations }] = texts || [{}];
    const [{ translation }] = translations || [{}];

    return translation;
  }

  sendNotification(params, devices) {
    return new Promise(async (resolve, reject) => {
      try {
        const content = await DailyHighlightService.run(params);

        const navigation = this.getNavigation(content);
        const body = this.getBody(content);

        let payload = {
          data: {
            content: JSON.stringify(content),
            navigation: JSON.stringify(navigation),
          },
          notification: {
            title: `Hi${
              params.user_name || ''
            }, your Daily Highlight is ready!`,
            body,
          },
        };

        const notification = await Notification.query().insert({
          title: payload.notification.title,
          message: payload.notification.body,
          data: JSON.stringify(payload.data),
          mobile_app_id: params.mobileAppId,
        });

        payload = {
          ...payload,
          data: { ...payload.data, notificationId: notification.id },
        };

        const config = {
          contentAvailable: true,
          priority: 'high',
        };

        let notifications = [];

        // const results = await Promise.allSettled();
        for (const { fcm_token } of devices) {
          try {
            const { results } = await firebase
              .messaging()
              .sendToDevice([fcm_token], payload, config);

            const [{ error, messageId }] = results;

            if (error) {
              throw new Error(error);
            }

            notifications = [
              ...notifications,
              { fcm_token, success: true, messageId },
            ];
          } catch (e) {
            notifications = [
              ...notifications,
              { fcm_token, success: false, error: e.message },
            ];
          }
        }

        resolve({ payload, notifications });
      } catch (e) {
        reject(e);
      }
    });
  }

  get key() {
    return 'HighlightPushNotification';
  }

  handle = ({ user, mobileAppId }) => {
    return new Promise(async (resolve, reject) => {
      const { userDevices, id: user_id, questionnaireAnswers } = user || {};
      const [{ id: questionnaire_answer_id }] = questionnaireAnswers || [{}];

      const params = {
        mobileAppId,
        user_id,
        questionnaire_answer_id,
        type: '',
        language: 'en',
        user_name: user?.first_name ? ` ${user?.first_name}` : '',
      };

      try {
        const highlightNotification = await this.sendNotification(
          params,
          userDevices
        );

        LogService.save({
          type: 'notification',
          user_id,
          request_data: { ...params, devices: userDevices },
          status_code: '200',
          response_data: highlightNotification,
        });

        resolve();
      } catch (e) {
        LogService.save({
          type: 'notification',
          user_id,
          request_data: { ...params, devices: userDevices },
          status_code: '400',
          response_data: e.message,
        });

        reject(e);
      }
    });
  };
}

module.exports = new HighlightNotificationJob();
